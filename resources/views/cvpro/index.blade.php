<!DOCTYPE html>
<html lang="fr" class="modern ">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title></title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <meta name="description"
    content="Créer votre CV en ligne et télécharger le directement ⭐ ! Choix de 32 modèles de CV professionnels ✅. Complétez, sélectionnez le modèle et téléchargez.">
  <meta name="keywords"
    content="CV, curriculum, vitae, exemple, faire, écrire, rédiger, postuler, candidature, écrit, comment, conseil, en ligne, fait, vitea, télécharger" />
  <meta name="application-name" content="&nbsp;" />
  <meta name="theme-color" content="#22a7f0">
  <meta name="robots" content="NOODP">
  <link rel="stylesheet" href="assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/css/min/jquery.fancybox.min.css" type="text/css" media="all" />
  <link href="https://fonts.googleapis.com/css?family=Noto+Serif:400" rel="stylesheet">
  <link rel="stylesheet" href="assets/css/min/cv-new.min.css" type="text/css" media="all" />
  <link rel="stylesheet" href="assets/css/min/extension.min.css" type="text/css" media="all" />
  <link rel="stylesheet" href="assets/css/style.css" type="text/css" media="all" />
  <link rel="stylesheet" href="#" type="text/css" media="all" />
  <link rel="stylesheet" href="../maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" type="text/css">
  <link rel="canonical" href="index.html" />

</head>
<body>
  <div class="wrapper ">
    <header role="banner">
      <div class="center">
        <a href="index.html" title="CV" class="site-logo steplink" data-next-step="1">
          <img src="assets/images/logo.png" srcset="" alt="CV"> </a>
        <nav role="navigation" class="pull-right">
          <ul>
            <li><a href="#" class="rnd-3 btn scroll-hiw">Comment cela fonctionne-t-il ?</a></li>
            <li><a href="tarifs.html" class="rnd-3 btn">Nos tarifs</a></li>
            <li><a href="#" title="FAQ" class="rnd-3 btn scroll-costs">Services</a></li>
            <li class="login"><span class="rnd-3">Se connecter</span>
              <form action="" method="post" class="rnd-3" id="loginbox">
                <p class="col-1">
                  <label for="login_email">Adresse e-mail</label>
                  <input type="text" class="rnd-6" id="login_email" name="email" value="" />
                </p>
                <p class="col-1">
                  <label for="login_password">Mot de passe</label>
                  <input type="password" id="login_password" name="password" value="" class="rnd-6" /> </p>
                <p class="col-2 remember-me">
                  <input type="checkbox" id="remember" name="remember_me" value="1" id="login_remember" />
                  <label class="show" for="remember">Se souvenir de moi</label>
                </p>
                <p class="pull-right forgot-password">
                  <a href="mot-de-passe-oublie.html" title="Mot de passe oublié ?">Mot de passe oublié ?</a>
                </p>
                <button id="login" type="submit" class="btn-blue-fill">Se connecter</button>
              </form>
            </li>
          </ul>
        </nav>
        <div class="user-info" style="display:none;"> <a href="index36b3.html">
            <span class="avatar rnd-34"
              style="background:#eff3f6 url('assets/images/default_female.png') no-repeat;"></span>
            <span class="user"> </span>
          </a>
        </div>
      </div>
    </header>
    <div class="hero hero-bg">
      <div class="center">
        <div class="col-1">
          <h1>Créez votre CV sur mesure en 3 étapes</h1>
          <p class="hero-intro">Plateforme de création de CV designs et efficaces. 
            Avec plus de 100 modèles de CV déjà créés CV-Pro Services est la
            boîte à outils essentielle pour une candidature réussie !</p>
        </div>
      </div>
    </div>
    <main role="main">
      <section id="examples">
        <div>
          <header>
            <h2>Nos modèles de CV</h2>
            <p>Vous voulez savoir à quoi peut ressembler votre CV ? Voici notre exemple de CV ci-dessous. Nous proposons
              huit modèles de CV différents, qui sont disponibles en quatre couleurs différentes. Vous avez donc le
              choix entre 32 variantes !</p>
          </header>
          <div class="col-1 mt-55">
            <div class="container">
              <div class="row">
                <div class="col-md-4 contour">
                  <div style="display:none;" id="cv-example-8" class="cv-holder"> <img
                      src="assets/images/examples/8/m4.png"
                      alt="cv exemple" /> <a onclick="$.fancybox.close()" title="Créer son CV"
                      class="btn-blue-fill scroll-form">Choisir le modèle</a><br>
                  </div> <a class="cv-fancybox" rel="gallery1" href="#cv-example-8"> <img
                      src="assets/images/examples/8/m4.png" style="width:100%;" alt="cv exemple" /> </a>
                </div>
                
                <div class="col-md-4 contour">
                  <div style="display:none;" id="cv-example-5" class="cv-holder"> <img
                      src="assets/images/examples/1/m15.png" alt="cv exemple" /> <a onclick="$.fancybox.close()"
                      title="Créer son CV" class="btn-blue-fill scroll-form">Choisir le modèle</a><br>
                  </div> <a class="cv-fancybox" rel="gallery1" href="#cv-example-5"> <img
                      src="assets/images/examples/1/m15.png" style="width:100%;" alt="cv exemple" /> </a>
                </div>

                <div class="col-md-4 contour">
                  <div style="display:none;" id="cv-example-8" class="cv-holder"> <img
                      src="assets/images/examples/8/cv-exemple-blue.png" alt="cv exemple" /> <a onclick="$.fancybox.close()"
                      title="Créer son CV" class="btn-blue-fill scroll-form">Choisir le modèle</a><br>
                  </div> <a class="cv-fancybox" rel="gallery1" href="#cv-example-8"> <img
                      src="assets/images/examples/8/cv-exemple-blue.png" style="width:100%;" alt="cv exemple" /> </a>
                </div>
                
                <div class="col-md-4 contour">
                  <div style="display:none;" id="cv-example-5" class="cv-holder"> <img
                      src="assets/images/examples/2/m41.png" alt="cv exemple" /> <a onclick="$.fancybox.close()"
                      title="Créer son CV" class="btn-blue-fill scroll-form">Choisir le modèle</a><br>
                  </div> <a class="cv-fancybox" rel="gallery1" href="#cv-example-5"> <img
                      src="assets/images/examples/2/m41.png" style="width:100%;" alt="cv exemple" /> </a>
                </div>
                
                <div class="col-md-4 contour">
                  <div style="display:none;" id="cv-example-8" class="cv-holder"> <img
                      src="assets/images/examples/3/m7.png" alt="cv exemple" /> <a onclick="$.fancybox.close()"
                      title="Créer son CV" class="btn-blue-fill scroll-form">Choisir le modèle</a><br>
                  </div> <a class="cv-fancybox" rel="gallery1" href="#cv-example-8"> <img
                      src="assets/images/examples/3/m7.png" style="width:100%;" alt="cv exemple" /> </a>
                </div>
                
                <div class="col-md-4 contour">
                  <div style="display:none;" id="cv-example-5" class="cv-holder"> <img
                      src="assets/images/examples/6/m29.png" alt="cv exemple" /> <a onclick="$.fancybox.close()"
                      title="Créer son CV" class="btn-blue-fill scroll-form">Choisir le modèle</a><br>
                  </div> <a class="cv-fancybox" rel="gallery1" href="#cv-example-5"> <img
                      src="assets/images/examples/6/m29.png" style="width:100%;" alt="cv exemple" /> </a>
                </div>
              </div>

            </div> 
          </div>
        </div>
      </section>
      <section class="how-it-works">
        <div class="container">
          <header id="how-it-works">
            <h2>Comment cela fonctionne-t-il ?</h2>
            <p>Créer un CV est simple. Commencez à entrer vos renseignements personnels. Ensuite, apportez du contenu au
              CV en nommant des éléments pertinents tels que les formations et l'expérience professionnelle. Enfin,
              choisissez un modèle de CV approprié. Votre CV est prêt à l'emploi !</p>
          </header>
          <div class="row row-features">
            <hr class="hr-features hidden-md hidden-sm" />
            <div class="col-md-4 text-center feature">
              <div class="icon-circle-lg"> <img src="assets/images/icons/icone/user.png" alt="icon" style="width: 100px;" /> </div>
              <h3>1. Identification</h3>
              <p class="text-left">Vous commencez par entrer vos données personnelles. Mentionnez à la fois vos données personnelles et
                vos coordonnées. En option, vous pouvez ajouter une photo et il est également possible d'importer votre
                CV actuel afin de copier le contenu.</p>
            </div>
            <div class="col-md-4 text-center feature">
              <div class="icon-circle-lg"> <img src="assets/images/icons/icone/book.png" alt="icon" style="width: 100px;" /> </div>
              <h3>2. Contenu</h3>
              <p class="text-left">Ensuite, faites un récapitulatif de vos formations, expériences professionnelles, compétences, langues
                et autres informations importantes qui constituent le contenu de votre curriculum vitae. Le CV est
                facilement personnalisable afin que vous puissiez toujours apporter des modifications par la suite.</p>
            </div>
            <div class="col-md-4 text-center feature">
              <div class="icon-circle-lg"> <img src="assets/images/icons/icone/file copie.png" alt="icon" style="width: 100px;" /> </div>
              <h3>3. Télécharger</h3>
              <p class="text-left">Après avoir complété le CV, choisissez un modèle de CV qui correspond à votre personnalité et à votre
                style. Huit exemples de CV sont présentés, tous sont disponibles en quatre couleurs différentes. Après
                paiement, votre compte est activé et votre CV peut être téléchargé immédiatement et adapté par la suite.
                Terminé !</p>
            </div>
          </div> <a href="#" title="Créer son CV" class="btn-blue-fill scroll-form btn-center">Créer son CV</a>
        </div>
      </section>
      <section id="used-by">
        <div class="container">
          <header>
            <h2>Pourquoi nous choisir?</h2>
          </header>
          <div class="row row-features">
            <div class="col-md-4 col-sm-6 feature feature-sm"> <img src="assets/images/icons/icone/coffee-cup.png" alt="icon" style="width: 100px;"/>
              <p>On s'occupe de tout. Concentrez-vous plutôt sur l'entretien</p>
            </div>
            <div class="col-md-4 col-sm-6 feature feature-sm"> <img src="assets/images/icons/icone/vintage.png" style="width: 100px;" alt="icon" />
              <p>Parce que vous êtes unique nous vous proposons un service personnalisé</p>
            </div>
            <div class="col-md-4 col-sm-6 feature feature-sm"> <img src="assets/images/icons/icone/sale.png" style="width: 100px;" alt="icon" />
              <p>Une offre claire, sans engagement et pas de frais cachés</p>
            </div>
            <div class="col-md-4 col-sm-6 feature feature-sm"> <img src="assets/images/icons/icone/mesure.png" style="width: 100px;" alt="icon" />
              <p>Du sur mesure réalisé en 48h ou un CV à traduire rapidement</p>
            </div>
            <div class="col-md-4 col-sm-6 feature feature-sm"> <img src="assets/images/icons/icone/document.png" style="width: 100px;" alt="icon" />
              <p>Lettre de Motivation entièrement gratuite et personnalisable sur MS Word.</p>
            </div>
            <div class="col-md-4 col-sm-6 feature feature-sm"> <img src="assets/images/icons/icone/main-up.png" style="width: 100px;" alt="icon" />
              <p>Des CV aux designs épurés et certifiés en amont par des recruteurs 
            </div>
          </div>
          
        </div>
      </section>
     
    </main>
    <footer class="clearfix" role="contentinfo">
      <div class="stats">
        <div class="container">
          <div class="col-md-6">
            <img src="assets/images/b2.png" alt="" style="width: 400px;"/>
          </div>
          <div class="col-md-6">
             <div class="row">
                <div class="col-md-6 col-sm-3 col-xs-6 text-left"> <span class="count">400</span> <span class="label text-left">Clients satisfaits</span>
                </div>
                <div class="col-md-6 col-sm-3 col-xs-6 text-left"> <span class="count">115</span> <span class="label text-left">CV traduits</span>
                </div>
                <div class="col-md-6 col-sm-3 col-xs-6 text-left"> <span class="count">32</span> <span class="label text-left">CV crées</span>
                </div>
                <div class="col-md-6 col-sm-3 col-xs-6 text-left"> <span class="count">1702</span> <span class="label text-left">Visiteurs/jr</span> 
                </div>
                <div class="col-md-6 col-sm-3 col-xs-6 text-left"> <span class="count">400</span> <span class="label text-left">ClientsModèles CV<br> disponible</span>
                </div>
                <div class="col-md-6 col-sm-3 col-xs-6 text-left"> <span class="count">115</span> <span class="label text-left">CV Nouveautés par mois</span>
                </div>
             </div>
          </div>
        </div>
      </div>

      <div class="footer-nav">
        <div class="container">
          <div class="row">
            <div class="col-md-3 footer-logo">
              <a href="index.html" title="CV" class="site-logo steplink" data-next-step="1">
                <img src="assets/images/logo.png" srcset="" alt="CV"> </a>
            </div>
            <div class="col-md-9">
              <div class="row">
                
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 footer-bottom">
              <hr /> <span class="copyright">&copy; 2019 cvwizard.fr</span> <a class="trustpilot-rating float-right"
                rel="nofollow" href="https://nl.trustpilot.com/review/cvwizard.fr" target="_blank">

                4,4/5 (1702 reviews)
              </a>
              <script type="application/ld+json">
                  {
                      "@context": "http://schema.org/",
                      "@type": "Website",
                      "name": "cvwizard.fr",
                      "aggregateRating": {
                          "@type": "AggregateRating",
                          "ratingValue": "4,4",
                          "bestRating": 5,
                          "ratingCount": 1702
                      }
                  }
              </script>
            </div>
          </div>
        </div>
      </div>
    </footer>

    <script src="assets/js/jquery-1.9.1.min.js"></script> 
    <script src="assets/js/bootstrap.min.js"></script>

    <script type="text/javascript">
      
    </script>
</body>

</html>