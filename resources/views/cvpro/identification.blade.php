<!DOCTYPE html>
<html lang="fr" class="modern ">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Faire un CV en ligne V wizard</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <meta name="description"
    content="Créer votre CV en ligne et télécharger le directement ⭐ ! Choix de 32 modèles de CV professionnels ✅. Complétez, sélectionnez le modèle et téléchargez.">
  <meta name="keywords"
    content="CV, curriculum, vitae, exemple, faire, écrire, rédiger, postuler, candidature, écrit, comment, conseil, en ligne, fait, vitea, télécharger" />
  <link rel="apple-touch-icon-precomposed" sizes="57x57" href="assets/images/favicons/apple-touch-icon-57x57.png" />
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/images/favicons/apple-touch-icon-114x114.png" />
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/images/favicons/apple-touch-icon-72x72.png" />
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/images/favicons/apple-touch-icon-144x144.png" />
  <link rel="apple-touch-icon-precomposed" sizes="60x60" href="assets/images/favicons/apple-touch-icon-60x60.png" />
  <link rel="apple-touch-icon-precomposed" sizes="120x120" href="assets/images/favicons/apple-touch-icon-120x120.png" />
  <link rel="apple-touch-icon-precomposed" sizes="76x76" href="assets/images/favicons/apple-touch-icon-76x76.png" />
  <link rel="apple-touch-icon-precomposed" sizes="152x152" href="assets/images/favicons/apple-touch-icon-152x152.png" />
  <link rel="icon" type="image/png" href="assets/images/favicons/favicon-196x196.png" sizes="196x196" />
  <link rel="icon" type="image/png" href="assets/images/favicons/favicon-96x96.png" sizes="96x96" />
  <link rel="icon" type="image/png" href="assets/images/favicons/favicon-32x32.png" sizes="32x32" />
  <link rel="icon" type="image/png" href="assets/images/favicons/favicon-16x16.png" sizes="16x16" />
  <link rel="icon" type="image/png" href="assets/images/favicons/favicon-128.png" sizes="128x128" />
  <meta name="application-name" content="&nbsp;" />
  <meta name="msapplication-TileColor" content="#FFFFFF" />
  <meta name="msapplication-TileImage" content="assets/images/favicons/ms-icon-144x144.png" />
  <meta name="msapplication-square70x70logo" content="assets/images/favicons/ms-icon-70x70.png" />
  <meta name="msapplication-square150x150logo" content="assets/images/favicons/ms-icon-150x150.png" />
  <meta name="msapplication-wide310x150logo" content="assets/images/favicons/ms-icon-310x150.png" />
  <meta name="msapplication-square310x310logo" content="assets/images/favicons/ms-icon-310x310.png" />
  <meta name="theme-color" content="#22a7f0">
  <meta name="robots" content="NOODP">
  <link rel="stylesheet" href="assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/css/min/jquery.fancybox.min.css" type="text/css" media="all" />
  <link href="https://fonts.googleapis.com/css?family=Noto+Serif:400" rel="stylesheet">
  <link rel="stylesheet" href="assets/css/min/cv-new.min.css" type="text/css" media="all" />
  <link rel="stylesheet" href="assets/css/min/extension.min.css" type="text/css" media="all" />
  <link rel="stylesheet" href="#" type="text/css" media="all" />
  <link rel="stylesheet" href="../maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" type="text/css">


</head>

<body>
 
  <div class="wrapper ">
    <header role="banner">
      <div class="center">
        <a href="index.html" title="CV wizard" class="site-logo steplink" data-next-step="1">
          <img src="assets/images/logo.png" srcset="/assets/images/logo.png 2x" alt="CV wizard"> </a>
        <nav role="navigation" class="pull-right">
          <ul>
            <li><a href="#" class="rnd-3 btn scroll-hiw">Comment cela fonctionne-t-il ?</a></li>
            <li><a href="#" class="rnd-3 btn scroll-costs">Prix</a></li>
            <li><a href="faq.html" title="FAQ" class="rnd-3 btn">FAQ</a></li>
            <li class="login"><span class="rnd-3">Se connecter</span>
              <form action="" method="post" class="rnd-3" id="loginbox">
                <p class="col-1">
                  <label for="login_email">Adresse e-mail</label>
                  <input type="text" class="rnd-6" id="login_email" name="email" value="" />
                </p>
                <p class="col-1">
                  <label for="login_password">Mot de passe</label>
                  <input type="password" id="login_password" name="password" value="" class="rnd-6" /> </p>
                <p class="col-2 remember-me">
                  <input type="checkbox" id="remember" name="remember_me" value="1" id="login_remember" />
                  <label class="show" for="remember">Se souvenir de moi</label>
                </p>
                <p class="pull-right forgot-password">
                  <a href="mot-de-passe-oublie.html" title="Mot de passe oublié ?">Mot de passe oublié ?</a>
                </p>
                <button id="login" type="submit" class="btn-blue-fill">Se connecter</button>
              </form>
            </li>
          </ul>
        </nav>
        <div class="user-info" style="display:none;"> <a href="index36b3.html">
            <span class="avatar rnd-34"
              style="background:#eff3f6 url('assets/images/default_female.png') no-repeat;"></span>
            <span class="user"> </span>
          </a>
        </div>
      </div>
    </header>


    <div class="hero hero-bg">
      <div class="center">
        <div class="col-1">
          <h1>Créez votre CV en 3 étapes</h1>
          <p class="hero-intro">Remplissez vos coordonnées, choisissez un modèle et téléchargez votre CV.<br /> Facile
            et rapide - prêt en 10 minutes. Commencez tout de suite !</p>
        </div>
      </div>
    </div>

    
    <main role="main">
      <script type="text/javascript">
        var js_lang_required_1 = 'Entrez votre prénom.';
        var js_lang_required_2 = 'Je voornaam is te kort';
        var js_lang_required_3 = 'Entrez votre nom de famille.';
        var js_lang_required_4 = 'Je achternaam is te kort';
        var js_lang_required_5 = 'Kies je geboortedatum';
        var js_lang_required_6 = 'Vul je geboorteplaats in.';
        var js_lang_required_7 = 'Entrez votre code postal.';
        var js_lang_required_8 = 'Huisnummer';
        var js_lang_required_9 = 'Saisissez votre numéro de téléphone.';
        var js_lang_required_10 = 'Vul een 10-cijferig nummer in';
        var js_lang_required_11 = 'Vul een 10-cijferig nummer in';
        var js_lang_required_12 = 'Entrez votre ville.';
        var js_lang_required_13 = 'Vul je nationaliteit in.';
        var js_lang_required_14 = 'Saisissez votre adresse e-mail.';
        var js_lang_required_15 = 'Entrez une adresse e-mail valide.';
        var js_lang_required_16 = 'Geboortedatum ongeldig (dd-mm-jjjj)';
        var js_lang_required_17 = 'Ongeldige postcode';
        var js_lang_required_18 = 'Vous avez déjà un CV. Connectez-vous.';
        var js_lang_required_19 = 'Faites un choix';
        var js_lang_required_20 = 'Entrez adresse (numéro et rue)';
        var feedback = false; 
      </script>

      <div id="form" class="step-1">
        <div class="center">
          <ul class="tabs">
            <li class="active">
                <a id="step_1_link" class="no-cursor" 
                    data-next-step="1" data-href="/Identification"
                    href="index.html" title="Identification" onclick="return false;">
                    <i class="rnd-20">
                        <span>1</span>
                    </i>
                    <br> 
                    Identification
                </a>
                <i class="overlay"></i>
            </li>

            <li>
                <a id="step_2_link" class="steplink" 
                    data-next-step="2" 
                    href="/contenu" title="Contenu">
                    <i class="rnd-20">
                        <span>2</span>
                    </i>
                    <br> 
                    Contenu
                </a>
                <i class="overlay"></i>
            </li>

            <li class="last">
                <a id="step_3_link" class="steplink" 
                    data-next-step="3" data-href="/telecharger"
                    href="index.html" 
                    title="Télécharger">
                    <i class="rnd-20">
                        <span>3</span>
                    </i><br> 
                    Télécharger
                </a>
                <i class="overlay"></i>
            </li>
          </ul>
          <div class="row">

          <form id="step_1" class="form-step" action="{{route('identification.store')}}" method="POST" enctype="multipart/form-data"
              autocomplete="off" autofill="off">
              
            @csrf
            <div class="col-md-4 col-sm-4 upload-photo">
              <div id="uploader" class="upload rnd-10">
                <div class="">
                  <div class="holder rnd-10">
                    <img src="assets/images/default-avatar.png" cropwidth="190" cropheight="190" id="crop-img"
                      class="rnd-10 cropimage" data-default="/assets/images/default-avatar.png" alt="avatar upload" />
                  </div>
                </div>
                <label class="show" for="avatar">
                  <span data-valid="Ajouter une photo" data-error="Fichier non valide"
                    class="btn btn-grey-fill rnd-3">
                    <i class="fa fa-camera"></i> Ajouter une photo</span>
                </label>
                <input type="hidden" name="remove_avatar" id="remove_avatar" style="display:none !important;">
                <input type="hidden" name="avatar_id" id="avatar_id" value="" style="display:none !important;">
                <input type="file" name="avatar" id="avatar" />
                <input type="text" id="rotation_val" name="rotation_val" value="0">
                <input type="hidden" id="avatarX" name="avatarX" value="" style="display:none !important;"> <input
                  type="hidden" id="avatarY" name="avatarY" value="" style="display:none !important;"> <input
                  type="hidden" id="avatarH" name="avatarH" value="" style="display:none !important;"> <input
                  type="hidden" id="avatarW" name="avatarW" value="" style="display:none !important;">
              </div>
              <a id="cv-import-btn" href="#cv-import-box" class="cv-uploader import-cv" title="">
                <i class="fa fa-upload"></i> Importer votre CV</a>
              <a style="display:none;" id="cv-import-btn" href="#cv-import-box-2" class="cv-uploader import-cv2"
                title="">
                <i class="fa fa-upload"></i> Test fancybox</a>
              <div class="language-holder" rel="txtTooltip" title="Modifiez la langue du CV" data-toggle="tooltip"
                data-placement="top">
                <i class="fa fa-globe" aria-hidden="true"></i>
                <select id="cv_language" name="cv_language" class="custom-select language-selector" placeholder="">
                  <option value="fr">français</option>
                  <option value="en">anglais</option>
                  
                </select>
                <i class="custom-select-arrow"></i>
              </div>
            </div>
            <div class="col-md-8 col-sm-8">
              <div class="row">
                <div class="form-group col-md-6 col-sm-6 col-sm-6">
                    <label class="input-label" data-fr="Prénom" for="firstname">
                        <span>Prénom</span>
                        <sup class="input-required">*</sup>
                    </label> 
                    
                    <input rel="txtTooltip-rthis" type="text" data-placement="top" 
                    class="data" id="firstname" name="firstname" value="" />
                </div>

                <div class="form-group col-md-6 col-sm-6 col-sm-6"> 
                    <label class="input-label" data-fr="Nom" for="lastname">
                        <span>Nom</span>
                        <sup class="input-required">*</sup>
                    </label> 
                    <input rel="txtTooltip-rthis" type="text" 
                        id="lastname" name="lastname" value="" /> 
                </div>

                <div class="form-group col-md-6 col-sm-6 col-sm-6"> 
                    <label class="input-label"
                        data-fr="Adresse (numéro et rue)" for="street">
                        <span>Adresse (numéro et rue)</span>
                    </label> 
                    <input type="text" id="street" name="street" value="" /> 
                </div>

                <div class="form-group col-md-6 col-sm-6">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-6"> 
                            <label class="input-label" data-fr="Code postal" for="zipcode">
                                <span>Code postal</span>
                            </label> 
                            <input type="text" id="zipcode" name="zipcode" value="" /> 
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6"> 
                            <label class="input-label" data-fr="Ville" for="city">
                                <span>Ville</span>
                                <sup class="input-required">*</sup>
                            </label> 
                            <input type="text" id="city" name="city" value="" />
                        </div>
                    </div>
                </div>

                <div class="form-group col-md-6 col-sm-6"> 
                    <label class="input-label"  data-fr="Téléphone" for="phonenumber">
                        <span>Téléphone</span>
                        <sup class="input-required">*</sup>
                    </label> 
                    <input type="text" id="phonenumber" name="phonenumber" value="" /> 
                </div>

                <div class="form-group col-md-6 col-sm-6"> 
                    <label class="input-label" data-fr="E-mail" for="email">
                        <span>E-mail</span>
                        <sup class="input-required">*</sup>
                    </label> 
                    <input type="email" id="email" name="email" value="" /> 
                </div>


                <div class="form-group col-md-6 col-sm-6 form-group-dob"> 
                    <label class="input-label" data-fr="Date de naissance" for="dob">
                        <span>Date de naissance</span>
                    </label>
                    <div class="dob-holder"> 
                        <select class="custom-select" name="dob_day">
                            <option data-nl="Dag" selected value="">Jour</option>
                            <option value="01">1</option>
                            <option value="02">2</option>
                            <option value="03">3</option>
                            <option value="04">4</option>
                            <option value="05">5</option>
                            <option value="06">6</option>
                            <option value="07">7</option>
                            <option value="08">8</option>
                            <option value="09">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                            <option value="21">21</option>
                            <option value="22">22</option>
                            <option value="23">23</option>
                            <option value="24">24</option>
                            <option value="25">25</option>
                            <option value="26">26</option>
                            <option value="27">27</option>
                            <option value="28">28</option>
                            <option value="29">29</option>
                            <option value="30">30</option>
                            <option value="31">31</option>
                        </select> 
                        <i class="custom-select-arrow dob-day"></i> 
                    </div>
                    <div class="dob-holder"> 
                        <select class="custom-select" name="dob_month">
                            <option data-nl="Maand" selected value="">Mois</option>
                            <option data-nl="jan" value="01">janvier</option>
                            <option data-nl="feb" value="02">février</option>
                            <option data-nl="mrt" value="03">mars</option>
                            <option data-nl="apr" value="04">avril</option>
                            <option data-nl="mei" value="05">mai</option>
                            <option data-nl="jun" value="06">juin</option>
                            <option data-nl="jul" value="07">juillet</option>
                            <option data-nl="aug" value="08">août</option>
                            <option data-nl="sep" value="09">septembre</option>
                            <option data-nl="okt" value="10">octobre</option>
                            <option data-nl="nov" value="11">novembre</option>
                            <option data-nl="dec" value="12">décembre</option>
                        </select> 
                        <i class="custom-select-arrow dob-month"></i> 
                    </div>
                    <div class="dob-holder"> 
                        <select class="custom-select" name="dob_year">
                            <option data-nl="Année" selected value="">Année</option>
                            <option value="2008">2008</option>
                            <option value="2007">2007</option>
                            <option value="2006">2006</option>
                            <option value="2005">2005</option>
                            <option value="2004">2004</option>
                            <option value="2003">2003</option>
                            <option value="2002">2002</option>
                            <option value="2001">2001</option>
                            <option value="2000">2000</option>
                            <option value="1999">1999</option>
                            <option value="1998">1998</option>
                            <option value="1997">1997</option>
                            <option value="1996">1996</option>
                            <option value="1995">1995</option>
                            <option value="1994">1994</option>
                            <option value="1993">1993</option>
                            <option value="1992">1992</option>
                            <option value="1991">1991</option>
                            <option value="1990">1990</option>
                            <option value="1989">1989</option>
                            <option value="1988">1988</option>
                            <option value="1987">1987</option>
                            <option value="1986">1986</option>
                            <option value="1985">1985</option>
                            <option value="1984">1984</option>
                            <option value="1983">1983</option>
                            <option value="1982">1982</option>
                            <option value="1981">1981</option>
                            <option value="1980">1980</option>
                            <option value="1979">1979</option>
                            <option value="1978">1978</option>
                            <option value="1977">1977</option>
                            <option value="1976">1976</option>
                            <option value="1975">1975</option>
                            <option value="1974">1974</option>
                            <option value="1973">1973</option>
                            <option value="1972">1972</option>
                            <option value="1971">1971</option>
                            <option value="1970">1970</option>
                            <option value="1969">1969</option>
                            <option value="1968">1968</option>
                            <option value="1967">1967</option>
                            <option value="1966">1966</option>
                            <option value="1965">1965</option>
                            <option value="1964">1964</option>
                            <option value="1963">1963</option>
                            <option value="1962">1962</option>
                            <option value="1961">1961</option>
                            <option value="1960">1960</option>
                            <option value="1959">1959</option>
                            <option value="1958">1958</option>
                            <option value="1957">1957</option>
                            <option value="1956">1956</option>
                            <option value="1955">1955</option>
                            <option value="1954">1954</option>
                            <option value="1953">1953</option>
                            <option value="1952">1952</option>
                            <option value="1951">1951</option>
                            <option value="1950">1950</option>
                            <option value="1949">1949</option>
                            <option value="1948">1948</option>
                            <option value="1947">1947</option>
                            <option value="1946">1946</option>
                            <option value="1945">1945</option>
                            <option value="1944">1944</option>
                            <option value="1943">1943</option>
                            <option value="1942">1942</option>
                        </select> 
                        <i class="custom-select-arrow dob-year"></i> 
                    </div>

                    <div style="display:none;" class="mb-date-input"> 
                        <input type="text" class="datepicker" id="dob"  name="dob" value="" data-date="" /> 
                        <i class="fa fa-calendar"></i> 
                    </div>
                </div>

                <div class="form-group col-md-6 col-sm-6"> 
                    <label class="input-label" data-fr="Statut marital" for="phonenumber">
                        <span>Statut marital</span>
                    </label> 
                    <input type="text" id="maritalstatus_text" name="maritalstatus_text" value="" placeholder="par ex. : Célibataire" /> 
                </div>

                <div class="form-group col-md-6 col-sm-6"> 
                    <label class="input-label" data-fr="Permis de conduire" for="drivinglicense_text"><span>Permis de conduire</span></label>
                    <input type="text" id="drivinglicense_text" name="drivinglicense_text" value=""
                    placeholder="par ex. : B, C" /> 
                </div>

                <div class="form-group nationality col-md-6 col-sm-6" style="display:none;"> 
                    <label  data-fr="Nationalité"  class="input-label" for="nationality">
                        <span>Nationalité</span> 
                        <i class="tools remove" aria-hidden="true">&times;</i> 
                    </label> 
                    <input type="text" id="nationality" name="nationality" value="" placeholder="" /> 
                </div>

                <div class="form-group dobcity col-md-6 col-sm-6" style="display:none;"> 
                    <label class="input-label" data-fr="Lieu de naissance"  for="dob_city"> 
                        <span>Lieu de naissance</span> 
                        <i class="tools remove" aria-hidden="true">&times;</i> 
                    </label> 
                    <input type="text" class="autocomplete" data-autocomplete-type="city" id="dob_city" name="dob_city" value="" />
                </div>

                  
                <div class="form-group url col-md-6 col-sm-6" style="display:none;"> 
                    <label class="input-label" data-fr="URL" data-es="Web" for="url"> 
                        <span>URL</span>
                        <i class="tools remove" aria-hidden="true">&times;</i> 
                    </label> 
                    <input type="text" id="url" name="url" value="" placeholder="" /> 
                </div>

                <div class="form-group linkedin col-md-6 col-sm-6" style="display:none;"> 
                    <label class="input-label" data-fr="LinkedIn"  for="linkedin"> 
                        <span>LinkedIn</span> 
                        <i class="tools remove" aria-hidden="true">&times;</i>
                    </label> 
                    <input type="text" id="linkedin" name="linkedin" value="" placeholder="" /> 
                </div>

                  
                <div class="form-group extra-field col-md-6 col-sm-6"> 
                  <label class="input-label">&nbsp;</label> 
                  <i class="plus"></i> 
                  <select class="extra_field custom-select" name="extra_field">
                    <option selected>+ Ajouter un champ</option>
                    <option data-fr="Nationalité"  value="nationality">Nationalité</option>
                    <option  data-fr="Lieu de naissance" value="dobcity">Lieu de naissance</option>
                    <option data-fr="URL" data-es="Web" value="url">URL</option>
                    <option  data-fr="LinkedIn" value="linkedin">LinkedIn</option>
                    
                  </select> 
                    <i class="custom-select-arrow"></i> 
                </div>

                <div class="form-group btn-holder"> 
                  <button type="submit" class="btn-blue-fill btn-next-step">
                    <i class="fa fa-lock"></i>Étape suivante<i class="fa fa-chevron-right"></i>
                  </button> 
                </div>
                
              </div>
            </div>
          </form>
            
            <form style="display:none;" id="cv-import-box" class="cv-box-info cv-holder" method="POST"
              enctype="multipart/form-data">
              <h2><i class="fa fa-upload"></i> Importer votre CV <i onclick="$.fancybox.close()"
                  class="close">&times;</i></h2>
              <div class="content">
                <div class="cv-upload-holder"> <label for="cv_import" class="btn-browse">Sélectionner</label> <label
                    data-valid='<i class="fa fa-upload"></i> Sélectionner un fichier <b>word</b> ou <b>pdf</b>'
                    data-error="Fichier non valable" for="cv_import" class="cv-import-input-label"> <i
                      class="fa fa-upload"></i> Sélectionner un fichier <b>word</b> ou <b>PDF</b> </label> <input
                    id="cv_import" type="file" name="cv_import"> <button type="submit" name="upload_cv"
                    id="upload_cv_btn">Upload CV</button> </div>
                <div class="upload-progress" style="display:none;"> <span style="width:5%;" data-width="5">0%</span>
                </div>
                <p>
                  Importez votre (ancien) CV pour que le contenu soit automatiquement collé dans les champs. Les champs
                  préalablement remplis seront remplacés par les informations de votre CV. </p>
                <ul>
                  <li><i>&bull;</i> Convient pour. <b>doc</b> et. <b>pdf</b>.</li>
                  <li><i>&bull;</i> Taille maximale du fichier 10 MB.</li>
                  <li><i>&bull;</i> L'exactitude et l'exhaustivité des champs collés doivent ensuite être vérifiées.
                  </li>
                </ul>
              </div>
            </form>
          </div>
        </div>
      </div>


      <section id="examples">
        <div>
          <header>
            <h2>Exemple de CV</h2>
            <p>Vous voulez savoir à quoi peut ressembler votre CV ? Voici notre exemple de CV ci-dessous. Nous proposons
              huit modèles de CV différents, qui sont disponibles en quatre couleurs différentes. Vous avez donc le
              choix entre 32 variantes !</p>
          </header>
          <div class="col-1 mt-55">
            <div class="slider-wrapper">
              <ul class="cv-slider">
                <li>
                  <div style="display:none;" id="cv-example-8" class="cv-holder"> <img
                      src="assets/images/examples/8/cv-exemple-blue.png" style="width:100%; height:auto;"
                      alt="cv exemple" /> <a onclick="$.fancybox.close()" title="Créer son CV"
                      class="btn-blue-fill scroll-form">Choisir le modèle</a><br>
                    <div class="controller rnd-6-b">
                      <ul>
                        <li><i class="colorpicker  active  rnd-10"
                            data-image="/assets/images/examples/8/cv-exemple-blue.png" style="background:#435270;"><i
                              class="circle rnd-6"></i></i></li>
                        <li><i class="colorpicker  rnd-10" data-image="/assets/images/examples/8/cv-exemple-vert.png"
                            style="background:#86b48f;"><i class="circle rnd-6"></i></i></li>
                        <li><i class="colorpicker  rnd-10" data-image="/assets/images/examples/8/cv-exemple-rouge.png"
                            style="background:#b45343;"><i class="circle rnd-6"></i></i></li>
                        <li><i class="colorpicker  rnd-10" data-image="/assets/images/examples/8/cv-exemple-marron.png"
                            style="background:#96795d;"><i class="circle rnd-6"></i></i></li>
                      </ul>
                    </div>
                  </div> <a class="cv-fancybox" rel="gallery1" href="#cv-example-8"> <img
                      src="assets/images/examples/8/cv-exemple-blue.png" alt="cv exemple" /> </a>
                </li>
                <li>
                  <div style="display:none;" id="cv-example-5" class="cv-holder"> <img
                      src="assets/images/examples/5/cv-exemple-gris.png" style="width:100%; height:auto;"
                      alt="cv exemple" /> <a onclick="$.fancybox.close()" title="Créer son CV"
                      class="btn-blue-fill scroll-form">Choisir le modèle</a><br>
                    <div class="controller rnd-6-b">
                      <ul>
                        <li><i class="colorpicker  active  rnd-10"
                            data-image="/assets/images/examples/5/cv-exemple-gris.png" style="background:#8e8e8e;"><i
                              class="circle rnd-6"></i></i></li>
                        <li><i class="colorpicker  rnd-10" data-image="/assets/images/examples/5/cv-exemple-blue.png"
                            style="background:#2f4768;"><i class="circle rnd-6"></i></i></li>
                        <li><i class="colorpicker  rnd-10" data-image="/assets/images/examples/5/cv-exemple-rouge.png"
                            style="background:#b13b3b;"><i class="circle rnd-6"></i></i></li>
                        <li><i class="colorpicker  rnd-10" data-image="/assets/images/examples/5/cv-exemple-vert.png"
                            style="background:#008080;"><i class="circle rnd-6"></i></i></li>
                      </ul>
                    </div>
                  </div> <a class="cv-fancybox" rel="gallery1" href="#cv-example-5"> <img
                      src="assets/images/examples/5/cv-exemple-gris.png" alt="cv exemple" /> </a>
                </li>
                <li>
                  <div style="display:none;" id="cv-example-4" class="cv-holder"> <img
                      src="assets/images/examples/4/cv-exemple-rouge.png" style="width:100%; height:auto;"
                      alt="cv exemple" /> <a onclick="$.fancybox.close()" title="Créer son CV"
                      class="btn-blue-fill scroll-form">Choisir le modèle</a><br>
                    <div class="controller rnd-6-b">
                      <ul>
                        <li><i class="colorpicker  active  rnd-10"
                            data-image="/assets/images/examples/4/cv-exemple-rouge.png" style="background:#b13b3b;"><i
                              class="circle rnd-6"></i></i></li>
                        <li><i class="colorpicker  rnd-10"
                            data-image="/assets/images/examples/4/cv-exemple-bleu-fonce.png"
                            style="background:#2f4768;"><i class="circle rnd-6"></i></i></li>
                        <li><i class="colorpicker  rnd-10" data-image="/assets/images/examples/4/cv-exemple-vert.png"
                            style="background:#429397;"><i class="circle rnd-6"></i></i></li>
                        <li><i class="colorpicker  rnd-10" data-image="/assets/images/examples/4/cv-exemple-blue.png"
                            style="background:#0092d7;"><i class="circle rnd-6"></i></i></li>
                      </ul>
                    </div>
                  </div> <a class="cv-fancybox" rel="gallery1" href="#cv-example-4"> <img
                      src="assets/images/examples/4/cv-exemple-rouge.png" alt="cv exemple" /> </a>
                </li>
                <li>
                  <div style="display:none;" id="cv-example-6" class="cv-holder"> <img
                      src="assets/images/examples/6/cv-exemple-blue.png" style="width:100%; height:auto;"
                      alt="cv exemple" /> <a onclick="$.fancybox.close()" title="Créer son CV"
                      class="btn-blue-fill scroll-form">Choisir le modèle</a><br>
                    <div class="controller rnd-6-b">
                      <ul>
                        <li><i class="colorpicker  active  rnd-10"
                            data-image="/assets/images/examples/6/cv-exemple-blue.png" style="background:#567895;"><i
                              class="circle rnd-6"></i></i></li>
                        <li><i class="colorpicker  rnd-10" data-image="/assets/images/examples/6/cv-exemple-rouge.png"
                            style="background:#b13b3b;"><i class="circle rnd-6"></i></i></li>
                        <li><i class="colorpicker  rnd-10" data-image="/assets/images/examples/6/cv-exemple-gris.png"
                            style="background:#8e8e8e;"><i class="circle rnd-6"></i></i></li>
                        <li><i class="colorpicker  rnd-10" data-image="/assets/images/examples/6/cv-exemple-marron.png"
                            style="background:#aa8e6d;"><i class="circle rnd-6"></i></i></li>
                      </ul>
                    </div>
                  </div> <a class="cv-fancybox" rel="gallery1" href="#cv-example-6"> <img
                      src="assets/images/examples/6/cv-exemple-blue.png" alt="cv exemple" /> </a>
                </li>
                <li>
                  <div style="display:none;" id="cv-example-7" class="cv-holder"> <img
                      src="assets/images/examples/7/cv-exemple-vert.png" style="width:100%; height:auto;"
                      alt="cv exemple" /> <a onclick="$.fancybox.close()" title="Créer son CV"
                      class="btn-blue-fill scroll-form">Choisir le modèle</a><br>
                    <div class="controller rnd-6-b">
                      <ul>
                        <li><i class="colorpicker  active  rnd-10"
                            data-image="/assets/images/examples/7/cv-exemple-vert.png" style="background:#429397;"><i
                              class="circle rnd-6"></i></i></li>
                        <li><i class="colorpicker  rnd-10" data-image="/assets/images/examples/7/cv-exemple-marron.png"
                            style="background:#90735f;"><i class="circle rnd-6"></i></i></li>
                        <li><i class="colorpicker  rnd-10" data-image="/assets/images/examples/7/cv-exemple-blue.png"
                            style="background:#2f4768;"><i class="circle rnd-6"></i></i></li>
                        <li><i class="colorpicker  rnd-10" data-image="/assets/images/examples/7/cv-exemple-rouge.png"
                            style="background:#7e1202;"><i class="circle rnd-6"></i></i></li>
                      </ul>
                    </div>
                  </div> <a class="cv-fancybox" rel="gallery1" href="#cv-example-7"> <img
                      src="assets/images/examples/7/cv-exemple-vert.png" alt="cv exemple" /> </a>
                </li>
                <li>
                  <div style="display:none;" id="cv-example-3" class="cv-holder"> <img
                      src="assets/images/examples/3/cv-exemple-noir.png" style="width:100%; height:auto;"
                      alt="cv exemple" /> <a onclick="$.fancybox.close()" title="Créer son CV"
                      class="btn-blue-fill scroll-form">Choisir le modèle</a><br>
                    <div class="controller rnd-6-b">
                      <ul>
                        <li><i class="colorpicker  active  rnd-10"
                            data-image="/assets/images/examples/3/cv-exemple-noir.png" style="background:#000;"><i
                              class="circle rnd-6"></i></i></li>
                        <li><i class="colorpicker  rnd-10" data-image="/assets/images/examples/3/cv-exemple-blue.png"
                            style="background:#0092d7;"><i class="circle rnd-6"></i></i></li>
                        <li><i class="colorpicker  rnd-10" data-image="/assets/images/examples/3/cv-exemple-gris.png"
                            style="background:#8e8e8e;"><i class="circle rnd-6"></i></i></li>
                        <li><i class="colorpicker  rnd-10" data-image="/assets/images/examples/3/cv-exemple-rouge.png"
                            style="background:#b13b3b;"><i class="circle rnd-6"></i></i></li>
                      </ul>
                    </div>
                  </div> <a class="cv-fancybox" rel="gallery1" href="#cv-example-3"> <img
                      src="assets/images/examples/3/cv-exemple-noir.png" alt="cv exemple" /> </a>
                </li>
                <li>
                  <div style="display:none;" id="cv-example-1" class="cv-holder"> <img
                      src="assets/images/examples/1/cv-exemple-blue.png" style="width:100%; height:auto;"
                      alt="cv exemple" /> <a onclick="$.fancybox.close()" title="Créer son CV"
                      class="btn-blue-fill scroll-form">Choisir le modèle</a><br>
                    <div class="controller rnd-6-b">
                      <ul>
                        <li><i class="colorpicker  active  rnd-10"
                            data-image="/assets/images/examples/1/cv-exemple-blue.png" style="background:#2374bb;"><i
                              class="circle rnd-6"></i></i></li>
                        <li><i class="colorpicker  rnd-10" data-image="/assets/images/examples/1/cv-exemple-rouge.png"
                            style="background:#B13B3B;"><i class="circle rnd-6"></i></i></li>
                        <li><i class="colorpicker  rnd-10"
                            data-image="/assets/images/examples/1/cv-exemple-bleu-fonce.png"
                            style="background:#2F4768;"><i class="circle rnd-6"></i></i></li>
                        <li><i class="colorpicker  rnd-10" data-image="/assets/images/examples/1/cv-exemple-gris.png"
                            style="background:#7e7e7e;"><i class="circle rnd-6"></i></i></li>
                      </ul>
                    </div>
                  </div> <a class="cv-fancybox" rel="gallery1" href="#cv-example-1"> <img
                      src="assets/images/examples/1/cv-exemple-blue.png" alt="cv exemple" /> </a>
                </li>
                <li>
                  <div style="display:none;" id="cv-example-2" class="cv-holder"> <img
                      src="assets/images/examples/2/cv-exemple-rouge.png" style="width:100%; height:auto;"
                      alt="cv exemple" /> <a onclick="$.fancybox.close()" title="Créer son CV"
                      class="btn-blue-fill scroll-form">Choisir le modèle</a><br>
                    <div class="controller rnd-6-b">
                      <ul>
                        <li><i class="colorpicker  active  rnd-10"
                            data-image="/assets/images/examples/2/cv-exemple-rouge.png" style="background:#B13B3B;"><i
                              class="circle rnd-6"></i></i></li>
                        <li><i class="colorpicker  rnd-10" data-image="/assets/images/examples/2/cv-exemple-gris.png"
                            style="background:#7e7e7e;"><i class="circle rnd-6"></i></i></li>
                        <li><i class="colorpicker  rnd-10"
                            data-image="/assets/images/examples/2/cv-exemple-bleu-fonce.png"
                            style="background:#2F4768;"><i class="circle rnd-6"></i></i></li>
                        <li><i class="colorpicker  rnd-10" data-image="/assets/images/examples/2/cv-exemple-blue.png"
                            style="background:#2374bb;"><i class="circle rnd-6"></i></i></li>
                      </ul>
                    </div>
                  </div> <a class="cv-fancybox" rel="gallery1" href="#cv-example-2"> <img
                      src="assets/images/examples/2/cv-exemple-rouge.png" alt="cv exemple" /> </a>
                </li>
              </ul> <span id="slider-prev"></span> <span id="slider-next"></span>
            </div> <a href="#" title="Créer son CV" class="btn-blue-fill scroll-form">Créer son CV</a>
          </div>
        </div>
      </section>
     
      
    </main>
    <footer class="clearfix" role="contentinfo">
        <div class="stats">
            <div class="container">
            <div class="col-md-6">
                <img src="assets/images/b2.png" alt="" style="width: 400px;"/>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-6 col-sm-3 col-xs-6 text-left"> <span class="count">400</span> <span class="label text-left">Clients satisfaits</span>
                    </div>
                    <div class="col-md-6 col-sm-3 col-xs-6 text-left"> <span class="count">115</span> <span class="label text-left">CV traduits</span>
                    </div>
                    <div class="col-md-6 col-sm-3 col-xs-6 text-left"> <span class="count">32</span> <span class="label text-left">CV crées</span>
                    </div>
                    <div class="col-md-6 col-sm-3 col-xs-6 text-left"> <span class="count">1702</span> <span class="label text-left">Visiteurs/jr</span> 
                    </div>
                    <div class="col-md-6 col-sm-3 col-xs-6 text-left"> <span class="count">400</span> <span class="label text-left">ClientsModèles CV<br> disponible</span>
                    </div>
                    <div class="col-md-6 col-sm-3 col-xs-6 text-left"> <span class="count">115</span> <span class="label text-left">CV Nouveautés par mois</span>
                    </div>
                </div>
            </div>
            </div>
        </div>
        <div class="footer-nav">
            <div class="container">
            <div class="row">
                <div class="col-md-3 footer-logo">
                <a href="index.html" title="CV" class="site-logo steplink" data-next-step="1">
                    <img src="assets/images/logo.png" srcset="" alt="CV"> </a>
                </div>
                <div class="col-md-9">
                <div class="row">
                    
                </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 footer-bottom">
                <hr /> <span class="copyright">&copy; 2019 cvwizard.fr</span> <a class="trustpilot-rating float-right"
                    rel="nofollow" href="https://nl.trustpilot.com/review/cvwizard.fr" target="_blank">

                    4,4/5 (1702 reviews)
                </a>
                <script type="application/ld+json">
                    {
                        "@context": "http://schema.org/",
                        "@type": "Website",
                        "name": "cvwizard.fr",
                        "aggregateRating": {
                            "@type": "AggregateRating",
                            "ratingValue": "4,4",
                            "bestRating": 5,
                            "ratingCount": 1702
                        }
                    }
                </script>
                </div>
            </div>
            </div>
        </div>
    </footer>
   
    

 <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js" type="text/javascript"></script>
    <script src="/assets/jquery.steps-1.1.0/jquery.steps.min.js"></script>
    <script src="/assets/js/jquery-ui.min.js" type="text/javascript"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/jquery.ui.touch-punch.min.js" type="text/javascript"></script>
    <script src="/assets/js/min/hammer.min.js" type="text/javascript"></script>
    <script src="/assets/js/min/jQueryRotate.min3860.js?v=1" type="text/javascript"></script>
   
    <script src="/assets/js/min/jquery.mousewheel-3.0.6.pack.min.js" type="text/javascript"></script>
    <script src="/assets/js/min/jQcrop.min.js" type="text/javascript"></script>
    <script src="/assets/js/jquery.inview.min.js"></script>
    <script src="/assets/js/jquery.bxslider.min.js" type="text/javascript"></script>
    <script src="/assets/js/min/jquery.fancybox.pack.min.js" type="text/javascript"></script>
    <script src="/assets/js/jquery.infieldlabel.min.js" type="text/javascript"></script>
    <script src="/assets/js/jquery.sumoselect.min.js" type="text/javascript"></script>
    <script src="/assets/js/device.min.js" type="text/javascript"></script>
    <script src="/assets/js/min/main.min.js" type="text/javascript"></script>
    <script src="/assets/js/min/step_1.min.js" type="text/javascript"></script>
    <script src="/assets/js/min/mobile.min.js" type="text/javascript"></script>
    
</body>

</html>