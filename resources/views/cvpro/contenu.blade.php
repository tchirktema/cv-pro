<!DOCTYPE html>
<html lang="fr" class="modern ">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Faire un CV en ligne V wizard</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <meta name="description"
    content="Créer votre CV en ligne et télécharger le directement ⭐ ! Choix de 32 modèles de CV professionnels ✅. Complétez, sélectionnez le modèle et téléchargez.">
  <meta name="keywords"
    content="CV, curriculum, vitae, exemple, faire, écrire, rédiger, postuler, candidature, écrit, comment, conseil, en ligne, fait, vitea, télécharger" />
  <link rel="apple-touch-icon-precomposed" sizes="57x57" href="assets/images/favicons/apple-touch-icon-57x57.png" />
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/images/favicons/apple-touch-icon-114x114.png" />
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/images/favicons/apple-touch-icon-72x72.png" />
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/images/favicons/apple-touch-icon-144x144.png" />
  <link rel="apple-touch-icon-precomposed" sizes="60x60" href="assets/images/favicons/apple-touch-icon-60x60.png" />
  <link rel="apple-touch-icon-precomposed" sizes="120x120" href="assets/images/favicons/apple-touch-icon-120x120.png" />
  <link rel="apple-touch-icon-precomposed" sizes="76x76" href="assets/images/favicons/apple-touch-icon-76x76.png" />
  <link rel="apple-touch-icon-precomposed" sizes="152x152" href="assets/images/favicons/apple-touch-icon-152x152.png" />
  <link rel="icon" type="image/png" href="assets/images/favicons/favicon-196x196.png" sizes="196x196" />
  <link rel="icon" type="image/png" href="assets/images/favicons/favicon-96x96.png" sizes="96x96" />
  <link rel="icon" type="image/png" href="assets/images/favicons/favicon-32x32.png" sizes="32x32" />
  <link rel="icon" type="image/png" href="assets/images/favicons/favicon-16x16.png" sizes="16x16" />
  <link rel="icon" type="image/png" href="assets/images/favicons/favicon-128.png" sizes="128x128" />
  <meta name="application-name" content="&nbsp;" />
  <meta name="msapplication-TileColor" content="#FFFFFF" />
  <meta name="msapplication-TileImage" content="assets/images/favicons/ms-icon-144x144.png" />
  <meta name="msapplication-square70x70logo" content="assets/images/favicons/ms-icon-70x70.png" />
  <meta name="msapplication-square150x150logo" content="assets/images/favicons/ms-icon-150x150.png" />
  <meta name="msapplication-wide310x150logo" content="assets/images/favicons/ms-icon-310x150.png" />
  <meta name="msapplication-square310x310logo" content="assets/images/favicons/ms-icon-310x310.png" />
  <meta name="theme-color" content="#22a7f0">
  <meta name="robots" content="NOODP">
  <link rel="stylesheet" href="assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/css/min/jquery.fancybox.min.css" type="text/css" media="all" />
  <link href="https://fonts.googleapis.com/css?family=Noto+Serif:400" rel="stylesheet">
  <link rel="stylesheet" href="assets/css/min/cv-new.min.css" type="text/css" media="all" />
  <link rel="stylesheet" href="assets/css/min/extension.min.css" type="text/css" media="all" />
  <link rel="stylesheet" href="#" type="text/css" media="all" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.css" type="text/css">


</head>

<body>
 
  <div class="wrapper ">
    <header role="banner">
      <div class="center">
        <a href="index.html" title="CV wizard" class="site-logo steplink" data-next-step="1">
          <img src="assets/images/logo.png" srcset="/assets/images/logo.png 2x" alt="CV wizard"> </a>
        <nav role="navigation" class="pull-right">
          <ul>
            <li><a href="#" class="rnd-3 btn scroll-hiw">Comment cela fonctionne-t-il ?</a></li>
            <li><a href="#" class="rnd-3 btn scroll-costs">Prix</a></li>
            <li><a href="faq.html" title="FAQ" class="rnd-3 btn">FAQ</a></li>
            <li class="login"><span class="rnd-3">Se connecter</span>
              <form action="" method="post" class="rnd-3" id="loginbox">
                <p class="col-1">
                  <label for="login_email">Adresse e-mail</label>
                  <input type="text" class="rnd-6" id="login_email" name="email" value="" />
                </p>
                <p class="col-1">
                  <label for="login_password">Mot de passe</label>
                  <input type="password" id="login_password" name="password" value="" class="rnd-6" /> </p>
                <p class="col-2 remember-me">
                  <input type="checkbox" id="remember" name="remember_me" value="1" id="login_remember" />
                  <label class="show" for="remember">Se souvenir de moi</label>
                </p>
                <p class="pull-right forgot-password">
                  <a href="mot-de-passe-oublie.html" title="Mot de passe oublié ?">Mot de passe oublié ?</a>
                </p>
                <button id="login" type="submit" class="btn-blue-fill">Se connecter</button>
              </form>
            </li>
          </ul>
        </nav>
        <div class="user-info" style="display:none;"> <a href="index36b3.html">
            <span class="avatar rnd-34"
              style="background:#eff3f6 url('assets/images/default_female.png') no-repeat;"></span>
            <span class="user"> </span>
          </a>
        </div>
      </div>
    </header>


    <div class="hero hero-bg">
      <div class="center">
        <div class="col-1">
          <h1>Créez votre CV en 3 étapes</h1>
          <p class="hero-intro">Remplissez vos coordonnées, choisissez un modèle et téléchargez votre CV.<br /> Facile
            et rapide - prêt en 10 minutes. Commencez tout de suite !</p>
        </div>
      </div>
    </div>

    
    <main role="main">
       <div id="form" class="step-2">
            <div class="center">
                <ul class="tabs">
                  <li><a id="step_1_link" class="steplink" data-next-step="1" data-href="/Identification" href="/" title="Identification"><i class="rnd-20"><span><i class="fa fa-check"></i></span></i><br> Identification</a><i class="overlay"></i></li>
                  <li class="active"><a id="step_2_link" class="no-cursor" data-next-step="2" href="/contenu" title="Contenu" onclick="return false;"><i class="rnd-20"><span>2</span></i><br> Contenu</a><i class="overlay"></i></li>
                  <li class="last "><a id="step_3_link" class="steplink" data-next-step="3" data-href="/telecharger" href="/telecharger" title="Télécharger"><i class="rnd-20"><span>3</span></i><br> Télécharger</a><i class="overlay"></i></li>
                </ul>
            </div>
        <form id="step_2" class="step-2" action="{{route('contenu.store')}}" method="POST">
            @csrf
            <div class="sortable">
                <div class="profile ordering-field" data-type="profile" style=" z-index:997;">
                    <div class="center">
                        <div class="form-col">
                            <h2 class="active">
                                <i class="icon-profile active"></i> Profil
                                <div class="RightFloat Commands"> 
                                    <i data-direction="up" class="sorter fa fa-chevron-up"></i> 
                                    <i data-direction="down" class="sorter fa fa-chevron-down"></i> 
                                </div>
                                <span class="section-counter"></span> <hr> 
                            </h2>
                            <div class="section">
                                <div class="form-row rnd-6 active origin  saved"> 
                                    <span class="clear-section">
                                        <i title="Supprimer" rel="txtTooltip" data-toggle="tooltip" data-placement="top" class="clear-section-row fa fa-times"></i>
                                    </span> 
                                    <i style="display:none;" data-direction="up" class="form-row-sorter fa fa-chevron-up" aria-hidden="true"></i> 
                                    <i style="display:none;" data-direction="down" class="form-row-sorter fa fa-chevron-down" aria-hidden="true"></i>
                                    <input type="hidden" class="type-row" value="profile" id="type-row-profile" />
                                    
                                    <p class="row full-width">
                                        <label for="profile" class="label">Description</label> 
                                        <span class="full"> 
                                            <textarea name="profile" id="profile1" class="form-control" rows="10" cols="80"></textarea> 
                                        </span> 
                                    </p>
                                    
                                    <input type="hidden" class="form-row-sort-holder" name="form_row_sorter[]" value="" />
                                    <hr> 
                                </div> 
                                <span class="hr-overlay"></span> 
                                <span class="form-row-tips open-tips" data-tip="profile">
                                    <i class="fa fa-info"></i> Conseils
                                </span> 
                            </div>
                        </div>
                    </div>
                    <div class="tip-banner" id="profile" style="display:none;">
                        <div class="center">
                            <h3>Conseils</h3>
                            <ul>
                                <li> 
                                    <i class="fa fa-check"></i> 
                                    <span>
                                        Décrivez votre profil en maximum 5 lignes de texte et placez-le en haut du CV avec les flèches.
                                    </span> 
                                </li>
                                <li> 
                                    <i class="fa fa-check"></i> 
                                    <span>
                                        Par exemple, indiquez brièvement qui vous êtes, ce que vous savez faire, quelles sont vos qualités personnelles, ce que vous recherchez et à quoi vous aspirez.
                                    </span> 
                                </li>
                                <li> 
                                    <i class="fa fa-check"></i> 
                                    <span>
                                        Assurez-vous que le profil est pertinent et qu'il apporte une valeur ajoutée au poste pour lequel vous souhaitez postuler.
                                    </span> 
                                </li>
                                <li> 
                                    <i class="fa fa-check"></i> 
                                    <span>
                                        Par exemple : « Je suis une secrétaire sérieuse et loyale, critique, intègre et énergique. Forte de plus de 10 ans d'expérience je peux être vos oreilles et vos yeux, je m'assure que votre travail est bien organisé, afin que vous ayez plus de temps pour vos priorités. »
                                    </span> 
                                </li>
                            </ul> 
                            <i class="fa fa-times close-tips" data-tip="profile"></i> 
                        </div>
                    </div>
                </div>

                <div class="education ordering-field" data-type="education" style="z-index:999;">
                    <div class="center">
                        <div class="form-col">
                            <h2 class=""> 
                                <div class="RightFloat Commands"> 
                                    <i data-direction="up" class="sorter fa fa-chevron-up"></i> 
                                    <i data-direction="down" class="sorter fa fa-chevron-down"></i> 
                                </div> 
                                <span class="section-counter"></span> 
                                <i class="icon-education "></i>
                                        Formations
                                <hr> 
                            </h2>

                            <div class="section" style="display:none;">
                                <div class="form-row last hold-validation untouched origin "> 
                                    <span class="remove-row">
                                        <i rel="txtTooltip" title="Supprimer" data-toggle="tooltip" data-placement="top" class="fa fa-times"></i>
                                    </span> 
                                    <span class="clear-section">
                                        <i rel="txtTooltip" title="Supprimer" data-toggle="tooltip" data-placement="top" class="clear-section-row fa fa-times"></i>
                                    </span> 
                                    <i style="display:none;" data-direction="up" class="form-row-sorter fa fa-chevron-up" aria-hidden="true"></i> 
                                    <i style="display:none;" data-direction="down" class="form-row-sorter fa fa-chevron-down" aria-hidden="true"></i>
                                  
                                   <input type="hidden" class="type-row" value="education" id="type-row-education" />
                                  
                                    <p class="row">
                                        <label for="education_1" class="label">Formation</label> 
                                        <span class="full"> 
                                          <label class="placeholder hide" for="education_1">par ex. : Médecine</label> 
                                          <input type="text" class=" raw" id="education_1" placeholder="par ex. : Médecine" class="study" name="edu_name[]" value="" /> 
                                        </span> 
                                    </p>
                                    <p class="row">
                                        <label for="institute_1" class="label">Etablissement</label> 
                                        <span class="half"> 
                                            <label class="placeholder hide" for="institute_1">par ex. : UPMC</label> 
                                            <input type="text" class=" raw" class="study" id="institute_1" placeholder="par ex. : UPMC" name="edu_institution[]" value="" /> 
                                        </span> 
                                        <span class="half"> 
                                            <input type="text" placeholder="Ville" class="study raw validate-no" name="edu_city[]" value="" /> 
                                        </span> 
                                    </p>
                            
                                    <p class="row">
                                        <label for="education_period_from_1" class="label">Date de début</label>
                                        <span class="half"> 
                                            <select name="edu_month_start[]" class="custom-select raw validate-no"> 
                                                <option value="dont_show_month" selected>Mois</option> 
                                                <option value="1">janvier</option> 
                                                <option value="2">février</option> 
                                                <option value="3">mars</option> 
                                                <option value="4">avril</option> 
                                                <option value="5">mai</option> 
                                                <option value="6">juin</option> 
                                                <option value="7">juillet</option> 
                                                <option value="8">août</option> 
                                                <option value="9">septembre</option> 
                                                <option value="10">octobre</option> 
                                                <option value="11">novembre</option> 
                                                <option value="12">décembre</option> 
                                            </select> 
                                            <i class="custom-select-arrow"></i> 
                                            <span class="select-bg"></span> 
                                        </span> 
                                        <span class="half"> 
                                            <select name="edu_year_start[]" class="custom-select raw validate-no"> 
                                                <option selected>Année</option> 
                                                <option value="2020">2020</option> 
                                                <option value="2019">2019</option> 
                                                <option value="2018">2018</option> 
                                                <option value="2017">2017</option> 
                                                <option value="2016">2016</option> 
                                                <option value="2015">2015</option> 
                                                <option value="2014">2014</option> 
                                                <option value="2013">2013</option> 
                                                <option value="2012">2012</option> 
                                                <option value="2011">2011</option> 
                                                <option value="2010">2010</option> 
                                                <option value="2009">2009</option> 
                                                <option value="2008">2008</option> <option value="2007">2007</option> <option value="2006">2006</option> <option value="2005">2005</option> <option value="2004">2004</option> <option value="2003">2003</option> <option value="2002">2002</option> <option value="2001">2001</option> <option value="2000">2000</option> <option value="1999">1999</option> <option value="1998">1998</option> <option value="1997">1997</option> <option value="1996">1996</option> <option value="1995">1995</option> <option value="1994">1994</option> <option value="1993">1993</option> <option value="1992">1992</option> <option value="1991">1991</option> <option value="1990">1990</option> <option value="1989">1989</option> <option value="1988">1988</option> <option value="1987">1987</option> <option value="1986">1986</option> <option value="1985">1985</option> <option value="1984">1984</option> <option value="1983">1983</option> <option value="1982">1982</option> <option value="1981">1981</option> <option value="1980">1980</option> <option value="1979">1979</option> <option value="1978">1978</option> <option value="1977">1977</option> <option value="1976">1976</option> <option value="1975">1975</option> <option value="1974">1974</option> <option value="1973">1973</option> <option value="1972">1972</option> <option value="1971">1971</option> <option value="1970">1970</option> <option value="1969">1969</option> <option value="1968">1968</option> <option value="1967">1967</option> <option value="1966">1966</option> <option value="1965">1965</option> <option value="1964">1964</option> <option value="1963">1963</option> <option value="1962">1962</option> </select> <i class="custom-select-arrow"></i> <span class="select-bg"></span> </span>
                                    </p>
                                    <p class="row">
                                        <label for="education_period_till_1" class="label">Date de fin</label> <span class="half"> <select name="edu_month_end[]" class="custom-select raw month-end-select validate-no"> <option value="dont_show_month" selected>Mois</option> <option value="currently">présent</option> <option value="1">janvier</option> <option value="2">février</option> <option value="3">mars</option> <option value="4">avril</option> <option value="5">mai</option> <option value="6">juin</option> <option value="7">juillet</option> <option value="8">août</option> <option value="9">septembre</option> <option value="10">octobre</option> <option value="11">novembre</option> <option value="12">décembre</option> </select> <i class="custom-select-arrow"></i> <span class="select-bg"></span> </span> <span class="half"> <select name="edu_year_end[]" class="custom-select raw year-end-select validate-no"> <option selected>Année</option> <option value="currently">présent</option> <option value="2025">2025</option> <option value="2024">2024</option> <option value="2023">2023</option> <option value="2022">2022</option> <option value="2021">2021</option> <option value="2020">2020</option> <option value="2019">2019</option> <option value="2018">2018</option> <option value="2017">2017</option> <option value="2016">2016</option> <option value="2015">2015</option> <option value="2014">2014</option> <option value="2013">2013</option> <option value="2012">2012</option> <option value="2011">2011</option> <option value="2010">2010</option> <option value="2009">2009</option> <option value="2008">2008</option> <option value="2007">2007</option> <option value="2006">2006</option> <option value="2005">2005</option> <option value="2004">2004</option> <option value="2003">2003</option> <option value="2002">2002</option> <option value="2001">2001</option> <option value="2000">2000</option> <option value="1999">1999</option> <option value="1998">1998</option> <option value="1997">1997</option> <option value="1996">1996</option> <option value="1995">1995</option> <option value="1994">1994</option> <option value="1993">1993</option> <option value="1992">1992</option> <option value="1991">1991</option> <option value="1990">1990</option> <option value="1989">1989</option> <option value="1988">1988</option> <option value="1987">1987</option> <option value="1986">1986</option> <option value="1985">1985</option> <option value="1984">1984</option> <option value="1983">1983</option> <option value="1982">1982</option> <option value="1981">1981</option> <option value="1980">1980</option> <option value="1979">1979</option> <option value="1978">1978</option> <option value="1977">1977</option> <option value="1976">1976</option> <option value="1975">1975</option> <option value="1974">1974</option> <option value="1973">1973</option> <option value="1972">1972</option> <option value="1971">1971</option> <option value="1970">1970</option> <option value="1969">1969</option> <option value="1968">1968</option> <option value="1967">1967</option> <option value="1966">1966</option> <option value="1965">1965</option> <option value="1964">1964</option> <option value="1963">1963</option> </select> <i class="custom-select-arrow"></i> <span class="select-bg"></span> </span>
                                    </p>
                                    <p class="row full-width optional-extra-description" style="display:none;">
                                        <label for="education_description" class="label">Description</label> 
                                        <span class="full"> 
                                            <textarea name="edu_description[]" id="education_ckeditor_1" class="form-control" rows="10" cols="80"></textarea> 
                                        </span> 
                                    </p> 
                                    <i class="add-extra-description">
                                        <i class="fa fa-ellipsis-h" aria-hidden="true"></i>
                                    </i>
                                    <input type="hidden" class="form-row-sort-holder" name="form_row_sorter[]" value="1" />
                                    <hr> 
                                </div> 
                                <span class="hr-overlay"></span> 
                                <span class="clone-form-row" data-section="education" title="">
                                    <i class="fa fa-plus"></i> Ajouter une formation
                                </span> 
                                <span class="form-row-tips open-tips" data-tip="education">
                                    <i class="fa fa-info"></i> Conseils
                                </span> 
                            </div>
                        </div>
                    </div>
                    <div class="tip-banner" id="education" style="display:none;">
                        <div class="center">
                            <h3>Conseils</h3>
                            <ul>
                                <li> <i class="fa fa-check"></i> <span>Ne citez pas les écoles primaires et secondaires à moins que ce ne soit votre dernier programme d'études.</span> </li>
                                <li> <i class="fa fa-check"></i> <span>Mentionnez uniquement les formations pertinentes ou celles qui ajoutent de la valeur à l'emploi pour lequel vous souhaitez postuler. À moins que vous n'ayez peu de formation ou d'expérience professionnelle, il s'agit là d'une excellente façon de « remplir » votre CV et de montrer que vous êtes actif.</span> </li>
                                <li> <i class="fa fa-check"></i> <span>Ajoutez des informations supplémentaires en cliquant sur ···.</span> </li>
                            </ul> <i class="fa fa-times close-tips" data-tip="education"></i> </div>
                    </div>
                  </div>
                  <div class="work ordering-field" data-type="work" style="z-index:997;">
                      <div class="center">
                          <div class="form-col">
                              <h2 class=""><i class="icon-work-experiance "></i> Expériences professionnelles
  <div class="RightFloat Commands"> <i data-direction="up" class="sorter fa fa-chevron-up"></i> <i data-direction="down" class="sorter fa fa-chevron-down"></i> </div><span class="section-counter"></span> <hr> </h2>
                              <div class="section" style="display:none;">
                                  <div class="form-row untouched hold-validation rnd-6 active origin"> <span class="remove-row"><i title="Supprimer" rel="txtTooltip" data-toggle="tooltip" data-placement="top" class="fa fa-times"></i></span> <span class="clear-section"><i title="Supprimer" rel="txtTooltip" data-toggle="tooltip" data-placement="top" class="clear-section-row fa fa-times"></i></span> <i style="display:none;" data-direction="up" class="form-row-sorter fa fa-chevron-up" aria-hidden="true"></i> <i style="display:none;" data-direction="down" class="form-row-sorter fa fa-chevron-down" aria-hidden="true"></i>
                                      <input type="hidden" class="type-row" value="work" id="type-row-work" />
                                      <p class="row">
                                          <label for="job_function_1" class="label">Poste</label> <span class="full"> <input type="text" class="rnd-6 raw autocomplete" data-autocomplete-type="work_function" id="job_function_1" name="work_function[]" value="" placeholder="" /> </span> </p>
                                      <p class="row">
                                          <label for="job_employer_1" class="label">Employeur</label> <span class="half"> <input type="text" class="rnd-6 raw" id="job_employer_1" name="work_employer[]" value="" /> </span> <span class="half"> <input type="text" class="rnd-6 raw" id="job_city_1" name="work_city[]" value="" placeholder="Ville" /> </span> </p>
                                      <p class="row">
                                          <label for="job_period_from_1" class="label">Date de début</label> <span class="half"> <select name="work_month_start[]" class="custom-select raw validate-no"> <option value="reset">Mois</option> <option value="1">janvier</option> <option value="2">février</option> <option value="3">mars</option> <option value="4">avril</option> <option value="5">mai</option> <option value="6">juin</option> <option value="7">juillet</option> <option value="8">août</option> <option value="9">septembre</option> <option value="10">octobre</option> <option value="11">novembre</option> <option value="12">décembre</option> </select> <i class="custom-select-arrow"></i> <span class="select-bg"></span> </span> <span class="half"> <select name="work_year_start[]" class="custom-select raw validate-no"> <option value="reset">Année</option> <option value="2020">2020</option> <option value="2019">2019</option> <option value="2018">2018</option> <option value="2017">2017</option> <option value="2016">2016</option> <option value="2015">2015</option> <option value="2014">2014</option> <option value="2013">2013</option> <option value="2012">2012</option> <option value="2011">2011</option> <option value="2010">2010</option> <option value="2009">2009</option> <option value="2008">2008</option> <option value="2007">2007</option> <option value="2006">2006</option> <option value="2005">2005</option> <option value="2004">2004</option> <option value="2003">2003</option> <option value="2002">2002</option> <option value="2001">2001</option> <option value="2000">2000</option> <option value="1999">1999</option> <option value="1998">1998</option> <option value="1997">1997</option> <option value="1996">1996</option> <option value="1995">1995</option> <option value="1994">1994</option> <option value="1993">1993</option> <option value="1992">1992</option> <option value="1991">1991</option> <option value="1990">1990</option> <option value="1989">1989</option> <option value="1988">1988</option> <option value="1987">1987</option> <option value="1986">1986</option> <option value="1985">1985</option> <option value="1984">1984</option> <option value="1983">1983</option> <option value="1982">1982</option> <option value="1981">1981</option> <option value="1980">1980</option> <option value="1979">1979</option> <option value="1978">1978</option> <option value="1977">1977</option> <option value="1976">1976</option> <option value="1975">1975</option> <option value="1974">1974</option> <option value="1973">1973</option> <option value="1972">1972</option> <option value="1971">1971</option> <option value="1970">1970</option> <option value="1969">1969</option> <option value="1968">1968</option> <option value="1967">1967</option> <option value="1966">1966</option> <option value="1965">1965</option> <option value="1964">1964</option> <option value="1963">1963</option> <option value="1962">1962</option> </select> <i class="custom-select-arrow"></i> <span class="select-bg"></span> </span>
                                      </p>
                                      <p class="row">
                                          <label for="job_period_till_1" class="label">Date de fin</label> <span class="half"> <select name="work_month_end[]" class="custom-select raw month-end-select validate-no"> <option value="reset">Mois</option> <option value="currently">présent</option> <option value="1">janvier</option> <option value="2">février</option> <option value="3">mars</option> <option value="4">avril</option> <option value="5">mai</option> <option value="6">juin</option> <option value="7">juillet</option> <option value="8">août</option> <option value="9">septembre</option> <option value="10">octobre</option> <option value="11">novembre</option> <option value="12">décembre</option> </select> <i class="custom-select-arrow"></i> <span class="select-bg"></span> </span> <span class="half"> <select name="work_year_end[]" class="custom-select raw year-end-select validate-no"> <option value="reset">Année</option> <option value="currently">présent</option> <option value="2025">2025</option> <option value="2024">2024</option> <option value="2023">2023</option> <option value="2022">2022</option> <option value="2021">2021</option> <option value="2020">2020</option> <option value="2019">2019</option> <option value="2018">2018</option> <option value="2017">2017</option> <option value="2016">2016</option> <option value="2015">2015</option> <option value="2014">2014</option> <option value="2013">2013</option> <option value="2012">2012</option> <option value="2011">2011</option> <option value="2010">2010</option> <option value="2009">2009</option> <option value="2008">2008</option> <option value="2007">2007</option> <option value="2006">2006</option> <option value="2005">2005</option> <option value="2004">2004</option> <option value="2003">2003</option> <option value="2002">2002</option> <option value="2001">2001</option> <option value="2000">2000</option> <option value="1999">1999</option> <option value="1998">1998</option> <option value="1997">1997</option> <option value="1996">1996</option> <option value="1995">1995</option> <option value="1994">1994</option> <option value="1993">1993</option> <option value="1992">1992</option> <option value="1991">1991</option> <option value="1990">1990</option> <option value="1989">1989</option> <option value="1988">1988</option> <option value="1987">1987</option> <option value="1986">1986</option> <option value="1985">1985</option> <option value="1984">1984</option> <option value="1983">1983</option> <option value="1982">1982</option> <option value="1981">1981</option> <option value="1980">1980</option> <option value="1979">1979</option> <option value="1978">1978</option> <option value="1977">1977</option> <option value="1976">1976</option> <option value="1975">1975</option> <option value="1974">1974</option> <option value="1973">1973</option> <option value="1972">1972</option> <option value="1971">1971</option> <option value="1970">1970</option> <option value="1969">1969</option> <option value="1968">1968</option> <option value="1967">1967</option> <option value="1966">1966</option> <option value="1965">1965</option> <option value="1964">1964</option> <option value="1963">1963</option> </select> <i class="custom-select-arrow"></i> <span class="select-bg"></span> </span>
                                      </p>
                                      <p class="row full-width">
                                          <label for="job_experiance" class="label">Description</label> <span class="full"> <textarea name="work_description[]" id="editor1" class="form-control" rows="10" cols="80"></textarea> </span> </p>
                                      <input type="hidden" class="form-row-sort-holder" name="form_row_sorter[]" value="1" />
                                      <hr> </div>
                                  <div class="clones work-experiance_clones"></div> <span class="hr-overlay"></span> <span class="clone-form-row" data-section="work-experiance" title=""><i class="fa fa-plus"></i> Ajouter une expérience professionnelle</span> <span class="form-row-tips open-tips" data-tip="work-experiance"><i class="fa fa-info"></i> Conseils</span> </div>
                          </div>
                      </div>
                      <div class="tip-banner" id="work-experiance" style="display:none;">
                          <div class="center">
                              <h3>Conseils</h3>
                              <ul>
                                  <li> <i class="fa fa-check"></i> <span>Décrivez aussi concrètement et clairement que possible les activités de l'entreprise, vos tâches/responsabilités les plus importantes et ce que vous avez accompli.</span> </li>
                                  <li> <i class="fa fa-check"></i> <span>Utilisez des mots positifs et un langage actif. Évitez le jargon et les abréviations.</span> </li>
                                  <li> <i class="fa fa-check"></i> <span>Vous avez une grande expérience professionnelle ? Ne mentionnez que les postes correspondant à l'emploi pour lequel vous souhaitez postuler.</span> </li>
                              </ul> <i class="fa fa-times close-tips" data-tip="work-experiance"></i> </div>
                      </div>
                  </div>
                  <div class="skills ordering-field" data-type="skills" style="z-index:994;">
                      <div class="center">
                          <div class="form-col">
                              <h2 class=""><i class="icon-skills "></i> Compétences
<div class="RightFloat Commands"> <i data-direction="up" class="sorter fa fa-chevron-up"></i> <i data-direction="down" class="sorter fa fa-chevron-down"></i> </div><span class="section-counter"></span> <hr> </h2>
                              <div class="section" style="display:none;">
                                  <div class="form-row hold-validation untouched rnd-6 active origin"> <span class="remove-row"><i title="Supprimer" data-toggle="tooltip" rel="txtTooltip" data-placement="top" class="fa fa-times"></i></span> <span class="clear-section"><i title="Supprimer" data-toggle="tooltip" rel="txtTooltip" data-placement="top" class="clear-section-row fa fa-times"></i></span> <i style="display:none;" data-direction="up" class="form-row-sorter fa fa-chevron-up" aria-hidden="true"></i> <i style="display:none;" data-direction="down" class="form-row-sorter fa fa-chevron-down" aria-hidden="true"></i>
                                      <input type="hidden" class="type-row" value="skills" id="type-row-skill" />
                                      <p class="row">
                                          <label class="label">Compétence</label> <span class="full"> <input type="text" class="rnd-6 raw" id="skill_name_1" placeholder="Par ex. : Microsoft Office" name="skill_skill[]" value="" /> </span> </p>
                                      <p class="row">
                                          <label for="section" class="label">Niveau</label> <span class="full"> <select name="skill_level[]" class="custom-select raw"> <option>Faites un choix</option> <option value="137">Expert</option> <option value="136">Niveau avanc&eacute;</option> <option value="164">Interm&eacute;diaire</option> <option value="167">D&eacute;butant</option> </select> <i class="custom-select-arrow"></i> <span class="select-bg"></span> </span>
                                      </p>
                                      <input type="hidden" class="form-row-sort-holder" name="form_row_sorter[]" value="1" />
                                      <hr> </div>
                                  <div class="clones skills_clones"></div> <span class="hr-overlay"></span> <span class="clone-form-row" data-section="skills" title=""><i class="fa fa-plus"></i> Ajouter une compétence</span> <span class="form-row-tips open-tips" data-tip="skills"><i class="fa fa-info"></i> Conseils</span> </div>
                          </div>
                      </div>
                      <div class="tip-banner" id="skills" style="display:none;">
                          <div class="center">
                              <h3>Conseils</h3>
                              <ul>
                                  <li> <i class="fa fa-check"></i> <span>Indiquez les compétences informatiques que vous maîtrisez et qui sont pertinentes ou ont une valeur ajoutée pour le poste pour lequel vous souhaitez postuler.</span> </li>
                                  <li> <i class="fa fa-check"></i> <span>Par exemple : Excel, Word, Photoshop, SPSS, HTML ou simplement un ordinateur.</span> </li>
                                  <li> <i class="fa fa-check"></i> <span>Le cas échéant, identifiez les compétences pertinentes pour l'exercice de la fonction ou ayant une valeur ajoutée.</span> </li>
                                  <li> <i class="fa fa-check"></i> <span>Par exemple : la moulure, la construction d'échafaudages ou le stucage.</span> </li>
                                  <li> <i class="fa fa-check"></i> <span>Vous voulez citer des caractéristiques ou des compétences personnelles ? Précisez-les dans la partie « Qualités » dans le menu déroulant en bas de la page.</span> </li>
                              </ul> <i class="fa fa-times close-tips" data-tip="skills"></i> </div>
                      </div>
                  </div>
                  <div class="language ordering-field" data-type="language" style="z-index:993;">
                      <div class="center">
                          <div class="form-col">
                              <h2 class=""><i class="icon-language "></i> Langues
<div class="RightFloat Commands"> <i data-direction="up" class="sorter fa fa-chevron-up"></i> <i data-direction="down" class="sorter fa fa-chevron-down"></i> </div><span class="section-counter"></span> <hr></h2>
                              <div class="section" style="display:none;">
                                  <div class="form-row untouched hold-validation origin rnd-6 active"> <span class="remove-row"><i title="Supprimer" data-toggle="tooltip" rel="txtTooltip" data-placement="top" class="fa fa-times"></i></span> <span class="clear-section"><i title="Supprimer" data-toggle="tooltip" rel="txtTooltip" data-placement="top" class="clear-section-row fa fa-times"></i></span> <i style="display:none;" data-direction="up" class="form-row-sorter fa fa-chevron-up" aria-hidden="true"></i> <i style="display:none;" data-direction="down" class="form-row-sorter fa fa-chevron-down" aria-hidden="true"></i>
                                      <input type="hidden" class="type-row" value="language" id="type-row-language" />
                                      <p class="row">
                                          <label for="language_name" class="label">Langue</label> <span class="full"> <input type="text" id="language_name_1" class="rnd-6 raw" name="language_language[]" value="" placeholder="" /> </span> </p>
                                      <p class="row">
                                          <label for="language_niveau_1" class="label">Niveau</label> <span class="full"> <select name="language_level[]" class="custom-select raw"> <option value="" selected="selected">Faites un choix</option> <option value="138">Langue maternelle</option> <option value="139">Bilingue</option> <option value="165">Courant</option> <option value="166">Interm&eacute;diaire</option> <option value="168">D&eacute;butant</option> </select> <i class="custom-select-arrow"></i> <span class="select-bg"></span> </span>
                                      </p>
                                      <input type="hidden" class="form-row-sort-holder" name="form_row_sorter[]" value="1" />
                                      <hr> </div>
                                  <div class="clones language_clones"></div> <span class="hr-overlay"></span> <span class="clone-form-row" data-section="language" title=""><i class="fa fa-plus"></i> Ajouter une langue</span> <span class="form-row-tips open-tips" data-tip="language"><i class="fa fa-info"></i> Conseils</span> </div>
                          </div>
                      </div>
                      <div class="tip-banner" id="language" style="display:none;">
                          <div class="center">
                              <h3>Conseils</h3>
                              <ul>
                                  <li> <i class="fa fa-check"></i> <span>Indiquez autant de langues que possible, comme votre langue maternelle, les langues que vous parlez couramment, bien ou de façon satisfaisante.</span> </li>
                                  <li> <i class="fa fa-check"></i> <span>Soyez honnête en entrant votre niveau de langue.</span> </li>
                              </ul> <i class="fa fa-times close-tips" data-tip="language"></i> </div>
                      </div>
                  </div>
                  <div class="hobbies ordering-field" data-type="hobbies" style="z-index:992;">
                      <div class="center">
                          <div class="form-col">
                              <h2 class=""><i class="icon-hobbies "></i>  Centres d'intérêt
<div class="RightFloat Commands"> <i data-direction="up" class="sorter fa fa-chevron-up"></i> <i data-direction="down" class="sorter fa fa-chevron-down"></i> </div><span class="section-counter"></span> <hr> </h2>
                              <div class="section" style="display:none;">
                                  <div class="form-row untouched hold-validation rnd-6 active origin"> <span class="remove-row"><i title="Supprimer" data-toggle="tooltip" rel="txtTooltip" data-placement="top" class="fa fa-times"></i></span> <span class="clear-section"><i title="Supprimer" data-toggle="tooltip" rel="txtTooltip" data-placement="top" class="clear-section-row fa fa-times"></i></span> <i style="display:none;" data-direction="up" class="form-row-sorter fa fa-chevron-up" aria-hidden="true"></i> <i style="display:none;" data-direction="down" class="form-row-sorter fa fa-chevron-down" aria-hidden="true"></i>
                                      <input type="hidden" class="type-row" value="hobbies" id="type-row-hobby" />
                                      <p class="row">
                                          <label for="hobby_name" class="label">Intérêt</label> <span class="full"> <input type="text" maxlength="125" class="rnd-6 raw" id="hobby_name_1" name="hobby_hobby[]" value="" placeholder="par ex. : la cuisine" /> </span> </p>
                                      <input type="hidden" class="form-row-sort-holder" name="form_row_sorter[]" value="1" />
                                      <hr> </div>
                                  <div class="clones hobbies_clones"></div> <span class="hr-overlay"></span> <span class="clone-form-row" data-section="hobbies" title=""><i class="fa fa-plus"></i> Ajouter un centre d'intérêt</span> <span class="form-row-tips open-tips" data-tip="hobbies"><i class="fa fa-info"></i> Conseils</span> </div>
                          </div>
                      </div>
                      <div class="tip-banner" id="hobbies" style="display:none;">
                          <div class="center">
                              <h3>Conseils</h3>
                              <ul>
                                  <li> <i class="fa fa-check"></i> <span>Identifiez les centres d’intérêt que correspondent à l'image que vous essayez de véhiculer en tant que candidat idéal.</span> </li>
                                  <li> <i class="fa fa-check"></i> <span>Indiquez jusqu'à 4 centres d’intérêt.</span> </li>
                                  <li> <i class="fa fa-check"></i> <span>Évitez les centres d’intérêt qui créent une image négative comme le poker, les passe-temps chauds, etc.</span> </li>
                                  <li> <i class="fa fa-check"></i> <span>Ajoutez les centres d’intérêt séparément et en mots-clés pour une représentation optimale sur le CV.</span> </li>
                              </ul> <i class="fa fa-times close-tips" data-tip="hobbies"></i> </div>
                      </div>
                  </div>
                  <div class="characteristics ordering-field" data-type="characteristics" style="display:none; z-index:992;">
                      <div class="center">
                          <div class="form-col">
                              <h2 class=""><i class="icon-characteristics "></i> Qualités
          <div class="RightFloat Commands"> <i data-direction="up" class="sorter fa fa-chevron-up"></i> <i data-direction="down" class="sorter fa fa-chevron-down"></i> </div> <span class="section-counter"></span> <hr> </h2>
                              <div class="section" style="display:none;">
                                  <div class="form-row untouched hold-validation rnd-6 active origin"> <span class="remove-row"><i title="Supprimer" data-toggle="tooltip" rel="txtTooltip" data-placement="top" class="fa fa-times"></i></span> <span class="clear-section"><i title="Supprimer" data-toggle="tooltip" rel="txtTooltip" data-placement="top" class="clear-section-row fa fa-times"></i></span> <i style="display:none;" data-direction="up" class="form-row-sorter fa fa-chevron-up" aria-hidden="true"></i> <i style="display:none;" data-direction="down" class="form-row-sorter fa fa-chevron-down" aria-hidden="true"></i>
                                      <input type="hidden" class="type-row" value="characteristics" id="type-row-characteristics" />
                                      <p class="row">
                                          <label for="characteristics_name" class="label">Qualité</label> <span class="full"> <input type="text" maxlength="125" class="rnd-6 raw" id="characteristics_name_1" name="characteristics_characteristics[]" value="" placeholder="par ex. : créatif" /> </span> </p>
                                      <input type="hidden" class="form-row-sort-holder" name="form_row_sorter[]" value="1" />
                                      <hr> </div> <span class="hr-overlay"></span> <span class="clone-form-row" data-section="characteristics" title=""><i class="fa fa-plus"></i> Ajouter une qualité</span> <span class="form-row-tips open-tips" data-tip="characteristics"><i class="fa fa-info"></i> Conseils</span> </div>
                          </div>
                      </div>
                      <div class="tip-banner" id="characteristics" style="display:none;">
                          <div class="center">
                              <h3>Conseils</h3>
                              <ul>
                                  <li> <i class="fa fa-check"></i> <span>Citez vos qualités, les caractéristiques et les compétences qui sont pertinentes ou qui ont une valeur ajoutée pour l'emploi pour lequel vous souhaitez postuler.</span> </li>
                                  <li> <i class="fa fa-check"></i> <span>Par exemple : créatif, orienté solution, positif, persévérant, fiable.</span> </li>
                              </ul> <i class="fa fa-times close-tips" data-tip="characteristics"></i> </div>
                      </div>
                  </div>
                  <div class="certificates ordering-field" data-type="certificates" style="display:none; z-index:998">
                      <div class="center">
                          <div class="form-col">
                              <h2 class=""><i class="icon-certificates "></i> Certifications
                              <div class="RightFloat Commands"> <i data-direction="up" class="sorter fa fa-chevron-up"></i> <i data-direction="down" class="sorter fa fa-chevron-down"></i> </div> <span class="section-counter"></span></span> <hr> </h2>
                              <div class="section" style="display:none;">
                                  <div class="form-row untouched hold-validation origin rnd-6 active"> <span class="remove-row">
                                      <i title="Supprimer" data-toggle="tooltip" rel="txtTooltip" data-placement="top" class="fa fa-times"></i></span> <span class="clear-section"><i title="Supprimer" data-toggle="tooltip" rel="txtTooltip" data-placement="top" class="clear-section-row fa fa-times"></i></span> <i style="display:none;" data-direction="up" class="form-row-sorter fa fa-chevron-up" aria-hidden="true"></i> <i style="display:none;" data-direction="down" class="form-row-sorter fa fa-chevron-down" aria-hidden="true"></i>
                                      <input type="hidden" class="type-row" value="certificates" id="type-row-course" />
                                      <p class="row">
                                          <label for="certificates_name_1" class="label">Certification</label> 
                                          <span class="full"> <input type="text" class="rnd-6 raw" id="certificates_name_1" name="certificates_name[]" value="" placeholder="" /> </span> </p>
                                      <p class="row">
                                          <label for="certificates_period_till_1" class="label"> Période</label> <span class="half"> <select name="certificates_year[]" class="custom-select raw year-end-select validate-no"> <option value="reset">Année</option> <option value="currently">présent</option> <option value="2025">2025</option> <option value="2024">2024</option> <option value="2023">2023</option> <option value="2022">2022</option> <option value="2021">2021</option> <option value="2020">2020</option> <option value="2019">2019</option> <option value="2018">2018</option> <option value="2017">2017</option> <option value="2016">2016</option> <option value="2015">2015</option> <option value="2014">2014</option> <option value="2013">2013</option> <option value="2012">2012</option> <option value="2011">2011</option> <option value="2010">2010</option> <option value="2009">2009</option> <option value="2008">2008</option> <option value="2007">2007</option> <option value="2006">2006</option> <option value="2005">2005</option> <option value="2004">2004</option> <option value="2003">2003</option> <option value="2002">2002</option> <option value="2001">2001</option> <option value="2000">2000</option> <option value="1999">1999</option> <option value="1998">1998</option> <option value="1997">1997</option> <option value="1996">1996</option> <option value="1995">1995</option> <option value="1994">1994</option> <option value="1993">1993</option> <option value="1992">1992</option> <option value="1991">1991</option> <option value="1990">1990</option> <option value="1989">1989</option> <option value="1988">1988</option> <option value="1987">1987</option> <option value="1986">1986</option> <option value="1985">1985</option> <option value="1984">1984</option> <option value="1983">1983</option> <option value="1982">1982</option> <option value="1981">1981</option> <option value="1980">1980</option> <option value="1979">1979</option> <option value="1978">1978</option> <option value="1977">1977</option> <option value="1976">1976</option> <option value="1975">1975</option> <option value="1974">1974</option> <option value="1973">1973</option> <option value="1972">1972</option> <option value="1971">1971</option> <option value="1970">1970</option> <option value="1969">1969</option> <option value="1968">1968</option> <option value="1967">1967</option> <option value="1966">1966</option> <option value="1965">1965</option> <option value="1964">1964</option> <option value="1963">1963</option> </select> <i class="custom-select-arrow"></i> <span class="select-bg"></span> </span> <span class="half"> <select name="certificates_finished[]" class="custom-select raw validate-no"> <option value="reset"> Obtenu ?</option> <option value="y">Oui</option> <option value="n">Non</option> <option value="c">En cours</option> <option value="nvt">N/A</option> </select> <i class="custom-select-arrow"></i> <span class="select-bg"></span> </span>
                                      </p>
                                      <p class="row full-width optional-extra-description" style="display:none;">
                                          <label for="certificates_description" class="label">Description</label> <span class="full"> <textarea name="certificates_description[]" id="certificates_ckeditor_1" class="form-control" rows="10" cols="80"></textarea> </span> </p> <i class="add-extra-description"><i class="fa fa-ellipsis-h" aria-hidden="true"></i></i>
                                      <input type="hidden" class="form-row-sort-holder" name="form_row_sorter[]" value="1" />
                                      <hr> </div> <span class="hr-overlay"></span> <span class="clone-form-row" data-section="certificates" title="add extra section"><i class="fa fa-plus"></i> Ajouter une certification</span> <span class="form-row-tips open-tips" data-tip="certificates"><i class="fa fa-info"></i> Conseils</span> </div>
                          </div>
                      </div>
                      <div class="tip-banner" id="certificates" style="display:none;">
                          <div class="center">
                              <h3>Conseils</h3>
                              <ul>
                                  <li> <i class="fa fa-check"></i> <span>Veuillez ne mentionner que les certificats qui sont pertinents ou qui ont une valeur ajoutée pour l'emploi pour lequel vous souhaitez postuler.</span> </li>
                                  <li> <i class="fa fa-check"></i> <span>Sélectionnez dans la période l'année de certification. </span> </li>
                                  <li> <i class="fa fa-check"></i> <span>Ajoutez des informations supplémentaires en cliquant sur ···.</span> </li>
                              </ul> <i class="fa fa-times close-tips" data-tip="certificates"></i> </div>
                      </div>
                  </div>
                  <div class="courses ordering-field" data-type="courses" style="display:none;  z-index:998">
                      <div class="center">
                          <div class="form-col">
                              <h2 class=""><i class="icon-courses "></i> Cours
<div class="RightFloat Commands"> <i data-direction="up" class="sorter fa fa-chevron-up"></i> <i data-direction="down" class="sorter fa fa-chevron-down"></i> </div><span class="section-counter"></span> <hr> </h2>
                              <div class="section" style="display:none;">
                                  <div class="form-row untouched hold-validation origin rnd-6 active"> <span class="remove-row"><i title="Supprimer" data-toggle="tooltip" rel="txtTooltip" data-placement="top" class="fa fa-times"></i></span> <span class="clear-section"><i title="Supprimer" data-toggle="tooltip" rel="txtTooltip" data-placement="top" class="clear-section-row fa fa-times"></i></span> <i style="display:none;" data-direction="up" class="form-row-sorter fa fa-chevron-up" aria-hidden="true"></i> <i style="display:none;" data-direction="down" class="form-row-sorter fa fa-chevron-down" aria-hidden="true"></i>
                                      <input type="hidden" class="type-row" value="courses" id="type-row-course" />
                                      <p class="row">
                                          <label for="courses_name_1" class="label">Cours</label> <span class="full"> <input type="text" class="rnd-6 raw" id="courses_name_1" name="course_name[]" value="" placeholder="" /> </span> </p>
                                      <p class="row">
                                          <label for="courses_diploma_1" class="label">Période</label> <span class="half"> <select name="course_year_end[]" class="custom-select raw year-end-select validate-no"> <option value="reset">Année</option> <option value="currently">présent</option> <option value="2025">2025</option> <option value="2024">2024</option> <option value="2023">2023</option> <option value="2022">2022</option> <option value="2021">2021</option> <option value="2020">2020</option> <option value="2019">2019</option> <option value="2018">2018</option> <option value="2017">2017</option> <option value="2016">2016</option> <option value="2015">2015</option> <option value="2014">2014</option> <option value="2013">2013</option> <option value="2012">2012</option> <option value="2011">2011</option> <option value="2010">2010</option> <option value="2009">2009</option> <option value="2008">2008</option> <option value="2007">2007</option> <option value="2006">2006</option> <option value="2005">2005</option> <option value="2004">2004</option> <option value="2003">2003</option> <option value="2002">2002</option> <option value="2001">2001</option> <option value="2000">2000</option> <option value="1999">1999</option> <option value="1998">1998</option> <option value="1997">1997</option> <option value="1996">1996</option> <option value="1995">1995</option> <option value="1994">1994</option> <option value="1993">1993</option> <option value="1992">1992</option> <option value="1991">1991</option> <option value="1990">1990</option> <option value="1989">1989</option> <option value="1988">1988</option> <option value="1987">1987</option> <option value="1986">1986</option> <option value="1985">1985</option> <option value="1984">1984</option> <option value="1983">1983</option> <option value="1982">1982</option> <option value="1981">1981</option> <option value="1980">1980</option> <option value="1979">1979</option> <option value="1978">1978</option> <option value="1977">1977</option> <option value="1976">1976</option> <option value="1975">1975</option> <option value="1974">1974</option> <option value="1973">1973</option> <option value="1972">1972</option> <option value="1971">1971</option> <option value="1970">1970</option> <option value="1969">1969</option> <option value="1968">1968</option> <option value="1967">1967</option> <option value="1966">1966</option> <option value="1965">1965</option> <option value="1964">1964</option> <option value="1963">1963</option> </select> <i class="custom-select-arrow"></i> <span class="select-bg"></span> </span> <span class="half"> <select name="course_finished[]" class="custom-select raw validate-no"> <option value="reset"> Obtenu?</option> <option value="y">Oui</option> <option value="n">Non</option> <option value="c">En cours</option> <option value="nvt">N/A</option> </select> <i class="custom-select-arrow"></i> <span class="select-bg"></span> </span>
                                      </p>
                                      <p class="row full-width optional-extra-description" style="display:none;">
                                          <label for="course_description" class="label">Description</label> <span class="full"> <textarea name="course_description[]" id="courses_ckeditor_1" class="form-control" rows="10" cols="80"></textarea> </span> </p> <i class="add-extra-description"><i class="fa fa-ellipsis-h" aria-hidden="true"></i></i>
                                      <input type="hidden" class="form-row-sort-holder" name="form_row_sorter[]" value="1" />
                                      <hr> </div>
                                  <div class="clones courses_clones"></div> <span class="hr-overlay"></span> <span class="clone-form-row" data-section="courses" title=""><i class="fa fa-plus"></i> Ajouter un cours</span> <span class="form-row-tips open-tips" data-tip="courses"><i class="fa fa-info"></i> Conseils</span> </div>
                          </div>
                      </div>
                      <div class="tip-banner" id="courses" style="display:none;">
                          <div class="center">
                              <h3>Conseils</h3>
                              <ul>
                                  <li> <i class="fa fa-check"></i> <span>Veuillez ne mentionner que les cours qui sont pertinents ou qui ont une valeur ajoutée pour l'emploi pour lequel vous souhaitez postuler.</span> </li>
                                  <li> <i class="fa fa-check"></i> <span>Pour la période, sélectionnez l'année de fin de la formation.</span> </li>
                                  <li> <i class="fa fa-check"></i> <span>Ajoutez des informations supplémentaires en cliquant sur ···. </span> </li>
                              </ul> <i class="fa fa-times close-tips" data-tip="courses"></i> </div>
                      </div>
                  </div>
                  <div class="itskills ordering-field" data-type="itskills" style="display:none; z-index:994;">
                      <div class="center">
                          <div class="form-col">
                              <h2 class=""><i class="icon-skills "></i> Informatique
          <div class="RightFloat Commands"> <i data-direction="up" class="sorter fa fa-chevron-up"></i> <i data-direction="down" class="sorter fa fa-chevron-down"></i> </div><span class="section-counter"></span> <hr> </h2>
                              <div class="section" style="display:none;">
                                  <div class="form-row hold-validation untouched rnd-6 active origin"> <span class="remove-row"><i title="Supprimer" data-toggle="tooltip" rel="txtTooltip" data-placement="top" class="fa fa-times"></i></span> <span class="clear-section"><i title="Supprimer" data-toggle="tooltip" rel="txtTooltip" data-placement="top" class="clear-section-row fa fa-times"></i></span> <i style="display:none;" data-direction="up" class="form-row-sorter fa fa-chevron-up" aria-hidden="true"></i> <i style="display:none;" data-direction="down" class="form-row-sorter fa fa-chevron-down" aria-hidden="true"></i>
                                      <input type="hidden" class="type-row" value="itskills" id="type-row-itskill" />
                                      <p class="row">
                                          <label class="label">Informatique</label> <span class="full"> <input type="text" class="rnd-6 raw" id="itskill_name_1" placeholder="par ex. : Microsoft Office" name="itskill_skill[]" value="" /> </span> </p>
                                      <p class="row">
                                          <label for="section" class="label">Niveau</label> <span class="full"> <select name="itskill_level[]" class="custom-select raw"> <option>Faites un choix</option> <option value="137">Expert</option> <option value="136">Niveau avanc&eacute;</option> <option value="164">Interm&eacute;diaire</option> <option value="167">D&eacute;butant</option> </select> <i class="custom-select-arrow"></i> <span class="select-bg"></span> </span>
                                      </p>
                                      <input type="hidden" class="form-row-sort-holder" name="form_row_sorter[]" value="1" />
                                      <hr> </div>
                                  <div class="clones itskills_clones"></div> <span class="hr-overlay"></span> <span class="clone-form-row" data-section="skills" title=""><i class="fa fa-plus"></i> Ajouter un outil informatique</span> <span class="form-row-tips open-tips" data-tip="skills"><i class="fa fa-info"></i> Conseils</span> </div>
                          </div>
                      </div>
                      <div class="tip-banner" id="itskills" style="display:none;">
                          <div class="center">
                              <h3>Conseils</h3>
                              <ul>
                                  <li> <i class="fa fa-check"></i> <span>Indiquez les compétences informatiques que vous maîtrisez et qui sont pertinentes ou ont une valeur ajoutée pour le poste pour lequel vous souhaitez postuler.</span> </li>
                                  <li> <i class="fa fa-check"></i> <span>Par exemple : Excel, Word, Photoshop, SPSS, HTML ou simplement un ordinateur.</span> </li>
                                  <li> <i class="fa fa-check"></i> <span>Le cas échéant, identifiez les compétences pertinentes pour l'exercice de la fonction ou ayant une valeur ajoutée.</span> </li>
                                  <li> <i class="fa fa-check"></i> <span>Par exemple : la moulure, la construction d'échafaudages ou le stucage.</span> </li>
                                  <li> <i class="fa fa-check"></i> <span>Vous voulez citer des caractéristiques ou des compétences personnelles ? Précisez-les dans la partie « Qualités » dans le menu déroulant en bas de la page.</span> </li>
                              </ul> <i class="fa fa-times close-tips" data-tip="itskills"></i> </div>
                      </div>
                  </div>
                  <div class="assets ordering-field" data-type="assets" style="display:none; z-index:992;">
                      <div class="center">
                          <div class="form-col">
                              <h2 class=""><i class="icon-hobbies "></i> Atouts
          <div class="RightFloat Commands"> <i data-direction="up" class="sorter fa fa-chevron-up"></i> <i data-direction="down" class="sorter fa fa-chevron-down"></i> </div><span class="section-counter"></span> <hr> </h2>
                              <div class="section" style="display:none;">
                                  <div class="form-row untouched hold-validation rnd-6 active origin"> <span class="remove-row"><i title="Supprimer" data-toggle="tooltip" rel="txtTooltip" data-placement="top" class="fa fa-times"></i></span> <span class="clear-section"><i title="Supprimer" data-toggle="tooltip" rel="txtTooltip" data-placement="top" class="clear-section-row fa fa-times"></i></span> <i style="display:none;" data-direction="up" class="form-row-sorter fa fa-chevron-up" aria-hidden="true"></i> <i style="display:none;" data-direction="down" class="form-row-sorter fa fa-chevron-down" aria-hidden="true"></i>
                                      <input type="hidden" class="type-row" value="assets" id="type-row-asset" />
                                      <p class="row">
                                          <label for="asset_name" class="label">Atout</label> <span class="full"> <input type="text" class="rnd-6 raw" id="asset_name_1" name="asset_asset[]" value="" placeholder="par ex. : créatif" /> </span> </p>
                                      <input type="hidden" class="form-row-sort-holder" name="form_row_sorter[]" value="1" />
                                      <hr> </div>
                                  <div class="clones assets_clones"></div> <span class="hr-overlay"></span> <span class="clone-form-row" data-section="assets" title=""><i class="fa fa-plus"></i> Ajouter un atout</span> <span class="form-row-tips open-tips" data-tip="assets"><i class="fa fa-info"></i> Conseils</span> </div>
                          </div>
                      </div>
                      <div class="tip-banner" id="assets" style="display:none;">
                          <div class="center">
                              <h3>Conseils</h3>
                              <ul>
                                  <li> <i class="fa fa-check"></i> <span>Citez vos qualités, les caractéristiques et les compétences qui sont pertinentes ou qui ont une valeur ajoutée pour l'emploi pour lequel vous souhaitez postuler.</span> </li>
                                  <li> <i class="fa fa-check"></i> <span>Par exemple : créatif, orienté solution, positif, persévérant, fiable.
</span> </li>
                              </ul> <i class="fa fa-times close-tips" data-tip="assets"></i> </div>
                      </div>
                  </div>
                  <div class="internships ordering-field" data-type="internships" style="display:none;z-index:996;">
                      <div class="center">
                          <div class="form-col">
                              <h2 class=""><i class="icon-internships "></i> Stages
  <div class="RightFloat Commands"> <i data-direction="up" class="sorter fa fa-chevron-up"></i> <i data-direction="down" class="sorter fa fa-chevron-down"></i> </div> <span class="section-counter"></span> <hr> </h2>
                              <div class="section" style="display:none;">
                                  <div class="form-row untouched hold-validation origin rnd-6 active"> <span class="remove-row"><i title="Supprimer" data-toggle="tooltip" rel="txtTooltip" data-placement="top" class="fa fa-times"></i></span> <span class="clear-section"><i title="Supprimer" data-toggle="tooltip" rel="txtTooltip" data-placement="top" class="clear-section-row fa fa-times"></i></span> <i style="display:none;" data-direction="up" class="form-row-sorter fa fa-chevron-up" aria-hidden="true"></i> <i style="display:none;" data-direction="down" class="form-row-sorter fa fa-chevron-down" aria-hidden="true"></i>
                                      <input type="hidden" class="type-row" value="internships" id="type-row-internship" />
                                      <p class="row">
                                          <label for="internship_function_1" class="label">Poste</label> <span class="full"> <input type="text" class="rnd-6 raw" id="internship_function_1" name="internship_function[]" value="" placeholder="" /> </span> </p>
                                      <p class="row">
                                          <label for="internship_employer_1" class="label">Employeur</label> <span class="half"> <input type="text" class="rnd-6 raw" id="internship_employer_1" name="internship_employer[]" value="" /> </span> <span class="half"> <input type="text" class="rnd-6 raw" id="internship_city_1" name="internship_city[]" value="" placeholder="Ville" /> </span> </p>
                                      <p class="row">
                                          <label for="internship_period_from_1" class="label">Date de début</label> <span class="half"> <select name="internship_month_start[]" class="custom-select raw validate-no"> <option value="reset">Mois</option> <option value="1">janvier</option> <option value="2">février</option> <option value="3">mars</option> <option value="4">avril</option> <option value="5">mai</option> <option value="6">juin</option> <option value="7">juillet</option> <option value="8">août</option> <option value="9">septembre</option> <option value="10">octobre</option> <option value="11">novembre</option> <option value="12">décembre</option> </select> <i class="custom-select-arrow"></i> <span class="select-bg"></span> </span> <span class="half"> <select name="internship_year_start[]" class="custom-select raw validate-no"> <option value="reset">Année</option> <option value="2020">2020</option> <option value="2019">2019</option> <option value="2018">2018</option> <option value="2017">2017</option> <option value="2016">2016</option> <option value="2015">2015</option> <option value="2014">2014</option> <option value="2013">2013</option> <option value="2012">2012</option> <option value="2011">2011</option> <option value="2010">2010</option> <option value="2009">2009</option> <option value="2008">2008</option> <option value="2007">2007</option> <option value="2006">2006</option> <option value="2005">2005</option> <option value="2004">2004</option> <option value="2003">2003</option> <option value="2002">2002</option> <option value="2001">2001</option> <option value="2000">2000</option> <option value="1999">1999</option> <option value="1998">1998</option> <option value="1997">1997</option> <option value="1996">1996</option> <option value="1995">1995</option> <option value="1994">1994</option> <option value="1993">1993</option> <option value="1992">1992</option> <option value="1991">1991</option> <option value="1990">1990</option> <option value="1989">1989</option> <option value="1988">1988</option> <option value="1987">1987</option> <option value="1986">1986</option> <option value="1985">1985</option> <option value="1984">1984</option> <option value="1983">1983</option> <option value="1982">1982</option> <option value="1981">1981</option> <option value="1980">1980</option> <option value="1979">1979</option> <option value="1978">1978</option> <option value="1977">1977</option> <option value="1976">1976</option> <option value="1975">1975</option> <option value="1974">1974</option> <option value="1973">1973</option> <option value="1972">1972</option> <option value="1971">1971</option> <option value="1970">1970</option> <option value="1969">1969</option> <option value="1968">1968</option> <option value="1967">1967</option> <option value="1966">1966</option> <option value="1965">1965</option> <option value="1964">1964</option> <option value="1963">1963</option> <option value="1962">1962</option> </select> <i class="custom-select-arrow"></i> <span class="select-bg"></span> </span>
                                      </p>
                                      <p class="row">
                                          <label for="internship_period_till_1" class="label">Date de fin</label> <span class="half"> <select name="internship_month_end[]" class="custom-select raw month-end-select validate-no"> <option value="reset">Mois</option> <option value="currently">présent</option> <option value="1">janvier</option> <option value="2">février</option> <option value="3">mars</option> <option value="4">avril</option> <option value="5">mai</option> <option value="6">juin</option> <option value="7">juillet</option> <option value="8">août</option> <option value="9">septembre</option> <option value="10">octobre</option> <option value="11">novembre</option> <option value="12">décembre</option> </select> <i class="custom-select-arrow"></i> <span class="select-bg"></span> </span> <span class="half"> <select name="internship_year_end[]" class="custom-select raw year-end-select validate-no"> <option value="reset">Année</option> <option value="currently">présent</option> <option value="2025">2025</option> <option value="2024">2024</option> <option value="2023">2023</option> <option value="2022">2022</option> <option value="2021">2021</option> <option value="2020">2020</option> <option value="2019">2019</option> <option value="2018">2018</option> <option value="2017">2017</option> <option value="2016">2016</option> <option value="2015">2015</option> <option value="2014">2014</option> <option value="2013">2013</option> <option value="2012">2012</option> <option value="2011">2011</option> <option value="2010">2010</option> <option value="2009">2009</option> <option value="2008">2008</option> <option value="2007">2007</option> <option value="2006">2006</option> <option value="2005">2005</option> <option value="2004">2004</option> <option value="2003">2003</option> <option value="2002">2002</option> <option value="2001">2001</option> <option value="2000">2000</option> <option value="1999">1999</option> <option value="1998">1998</option> <option value="1997">1997</option> <option value="1996">1996</option> <option value="1995">1995</option> <option value="1994">1994</option> <option value="1993">1993</option> <option value="1992">1992</option> <option value="1991">1991</option> <option value="1990">1990</option> <option value="1989">1989</option> <option value="1988">1988</option> <option value="1987">1987</option> <option value="1986">1986</option> <option value="1985">1985</option> <option value="1984">1984</option> <option value="1983">1983</option> <option value="1982">1982</option> <option value="1981">1981</option> <option value="1980">1980</option> <option value="1979">1979</option> <option value="1978">1978</option> <option value="1977">1977</option> <option value="1976">1976</option> <option value="1975">1975</option> <option value="1974">1974</option> <option value="1973">1973</option> <option value="1972">1972</option> <option value="1971">1971</option> <option value="1970">1970</option> <option value="1969">1969</option> <option value="1968">1968</option> <option value="1967">1967</option> <option value="1966">1966</option> <option value="1965">1965</option> <option value="1964">1964</option> <option value="1963">1963</option> </select> <i class="custom-select-arrow"></i> <span class="select-bg"></span> </span>
                                      </p>
                                      <p class="row full-width">
                                          <label for="internship_experiance_1" class="label">Description</label> <span class="full"><textarea id="internship_1" name="internship_description[]" class="form-control" rows="10" cols="80"></textarea> </span> </p>
                                      <input type="hidden" class="form-row-sort-holder" name="form_row_sorter[]" value="1" />
                                      <hr> </div>
                                  <div class="clones internships_clones"></div> <span class="hr-overlay"></span> <span class="clone-form-row" data-section="internships" title=""><i class="fa fa-plus"></i> Ajouter un stage</span> <span class="form-row-tips open-tips" data-tip="internships"><i class="fa fa-info"></i> Conseils</span> </div>
                          </div>
                      </div>
                      <div class="tip-banner" id="internships" style="display:none;">
                          <div class="center">
                              <h3>Conseils</h3>
                              <ul>
                                  <li> <i class="fa fa-check"></i> <span>Décrivez aussi concrètement et clairement que possible les activités de l'entreprise, vos tâches/responsabilités les plus importantes et ce que vous avez accompli.</span> </li>
                                  <li> <i class="fa fa-check"></i> <span>Utilisez des mots positifs et un langage actif. Évitez le jargon et les abréviations.</span> </li>
                                  <li> <i class="fa fa-check"></i> <span>Vous avez fait beaucoup de stages ? Ne mentionnez que les stages correspondant à l'emploi pour lequel vous souhaitez postuler.</span> </li>
                              </ul> <i class="fa fa-times close-tips" data-tip="internships"></i> </div>
                      </div>
                  </div>
                  <div class="additional ordering-field" data-type="additional" style="display:none; z-index:991;">
                      <div class="center">
                          <div class="form-col">
                              <h2 class=""><i class="icon-extra "></i> Activités extra-professionnelles
  <div class="RightFloat Commands"> <i data-direction="up" class="sorter fa fa-chevron-up"></i> <i data-direction="down" class="sorter fa fa-chevron-down"></i> </div><span class="section-counter"></span> <hr> </h2>
                              <div class="section" style="display:none;">
                                  <div class="form-row rnd-6   saved"> <span class="clear-section"><i title="Supprimer" rel="txtTooltip" data-toggle="tooltip" data-placement="top" class="clear-section-row fa fa-times"></i></span> <i style="display:none;" data-direction="up" class="form-row-sorter fa fa-chevron-up" aria-hidden="true"></i> <i style="display:none;" data-direction="down" class="form-row-sorter fa fa-chevron-down" aria-hidden="true"></i>
                                      <input type="hidden" class="type-row" value="additional" id="type-row-additional" />
                                      <p class="row full-width">
                                          <label for="additional" class="label">Description</label> <span class="full"> <textarea name="additional" id="additional1" class="form-control" rows="10" cols="80" placeholder=""></textarea> </span> </p>
                                      <input type="hidden" class="form-row-sort-holder" name="form_row_sorter[]" value="" />
                                      <hr> </div> <span class="hr-overlay"></span> <span class="form-row-tips open-tips" data-tip="additional"><i class="fa fa-info"></i> Conseils</span> </div>
                          </div>
                      </div>
                      <div class="tip-banner" id="additional" style="display:none;">
                          <div class="center">
                              <h3>Conseils</h3>
                              <ul>
                                  <li> <i class="fa fa-check"></i> <span>Mentionnez uniquement les activités extra-professionnelles, tels que les loisirs, le travail pour les associations ou le bénévolat qui sont pertinentes ou ont une valeur ajoutée pour l'emploi pour lequel vous souhaitez postuler.</span> </li>
                                  <li> <i class="fa fa-check"></i> <span>Citez seulement les activités extra-professionnelles récentes des dernières années. À moins que cette activité extra-professionnelle ne soit pertinente ou n'ajoute de la valeur à l'emploi pour lequel vous souhaitez postuler.</span> </li>
                              </ul> <i class="fa fa-times close-tips" data-tip="additional"></i> </div>
                      </div>
                  </div>
                  <div class="references ordering-field" data-type="references" style="display:none; z-index:991;">
                      <div class="center">
                          <div class="form-col">
                              <h2 class=""><i class="icon-references "></i> Références
  <div class="RightFloat Commands"> <i data-direction="up" class="sorter fa fa-chevron-up"></i> <i data-direction="down" class="sorter fa fa-chevron-down"></i> </div><span class="section-counter"></span> <hr> </h2>
                              <div class="section" style="display:none;">
                                  <div class="form-row rnd-6   saved"> <span class="clear-section"><i title="Supprimer" rel="txtTooltip" data-toggle="tooltip" data-placement="top" class="clear-section-row fa fa-times"></i></span> <i style="display:none;" data-direction="up" class="form-row-sorter fa fa-chevron-up" aria-hidden="true"></i> <i style="display:none;" data-direction="down" class="form-row-sorter fa fa-chevron-down" aria-hidden="true"></i>
                                      <input type="hidden" class="type-row" value="references" id="type-row-references" />
                                      <p class="row full-width">
                                          <label for="reference_text" class="label">Description</label> <span class="full"> <textarea name="reference_text" id="reference_text1" class="form-control" rows="10" cols="80" ></textarea> </span> </p>
                                      <input type="hidden" class="form-row-sort-holder" name="form_row_sorter[]" value="" />
                                      <hr> </div> <span class="hr-overlay"></span> <span class="form-row-tips open-tips" data-tip="references"><i class="fa fa-info"></i> Conseils</span> </div>
                          </div>
                      </div>
                      <div class="tip-banner" id="references" style="display:none;">
                          <div class="center">
                              <h3>Conseils</h3>
                              <ul>
                                  <li> <i class="fa fa-check"></i> <span>Citez uniquement les références si on vous les demande.</span> </li>
                                  <li> <i class="fa fa-check"></i> <span>Si vous possédez des références, veuillez sélectionner « Références sur demande ».</span> </li>
                                  <li> <i class="fa fa-check"></i> <span>Si vous n'êtes pas en possession de références et qu'on ne vous en réclame pas, omettez cette partie.</span> </li>
                                  <li> <i class="fa fa-check"></i> <span>Demandez toujours à l'avance la permission de votre personne de référence.</span> </li>
                              </ul> <i class="fa fa-times close-tips" data-tip="references"></i> </div>
                      </div>
                  </div>
                  <div class="goal ordering-field" data-type="goal" style="display:none; z-index:991;">
                      <div class="center">
                          <div class="form-col">
                              <h2 class=""><i class="icon-extra "></i> Objectif
          <div class="RightFloat Commands"> <i data-direction="up" class="sorter fa fa-chevron-up"></i> <i data-direction="down" class="sorter fa fa-chevron-down"></i> </div><span class="section-counter"></span> <hr> </h2>
                              <div class="section" style="display:none;">
                                  <div class="form-row rnd-6   saved"> <span class="clear-section"><i title="Supprimer" rel="txtTooltip" data-toggle="tooltip" data-placement="top" class="clear-section-row fa fa-times"></i></span> <i style="display:none;" data-direction="up" class="form-row-sorter fa fa-chevron-up" aria-hidden="true"></i> <i style="display:none;" data-direction="down" class="form-row-sorter fa fa-chevron-down" aria-hidden="true"></i>
                                      <input type="hidden" class="type-row" value="goal" id="type-row-goal" />
                                      <p class="row full-width">
                                          <label for="goal" class="label">Description</label> <span class="full"> <textarea name="goal" id="goal1" class="form-control" rows="10" cols="80" placeholder=""></textarea> </span> </p>
                                      <input type="hidden" class="form-row-sort-holder" name="form_row_sorter[]" value="" />
                                      <hr> </div> <span class="hr-overlay"></span> <span class="form-row-tips open-tips" data-tip="goal"><i class="fa fa-info"></i> Conseils</span> </div>
                          </div>
                      </div>
                      <div class="tip-banner" id="goal" style="display:none;">
                          <div class="center">
                              <h3>Conseils</h3>
                              <ul>
                                  <li> <i class="fa fa-check"></i> <span>Décrivez votre objectif en maximum 5 lignes de texte et placez-le en haut du CV avec les flèches.</span> </li>
                                  <li> <i class="fa fa-check"></i> <span>Par exemple, indiquez brièvement qui vous êtes, ce que vous savez faire, quelles sont vos qualités personnelles, ce que vous recherchez et à quoi vous aspirez.</span> </li>
                                  <li> <i class="fa fa-check"></i> <span>Assurez-vous que le objectif est pertinent et qu'il apporte une valeur ajoutée au poste pour lequel vous souhaitez postuler.</span> </li>
                                  <li> <i class="fa fa-check"></i> <span>Par exemple : « Je suis une secrétaire sérieuse et loyale, critique, intègre et énergique. Forte de plus de 10 ans d'expérience je peux être vos oreilles et vos yeux, je m'assure que votre travail est bien organisé, afin que vous ayez plus de temps pour vos priorités. »</span> </li>
                              </ul> <i class="fa fa-times close-tips" data-tip="goal"></i> </div>
                      </div>
                  </div>
                  <div class="achievements ordering-field" data-type="achievements" style="display:none; z-index:997;">
                      <div class="center">
                          <div class="form-col">
                              <h2 class=""><i class="icon-achievements "></i> Divers
          <div class="RightFloat Commands"> <i data-direction="up" class="sorter fa fa-chevron-up"></i> <i data-direction="down" class="sorter fa fa-chevron-down"></i> </div><span class="section-counter"></span> <hr> </h2>
                              <div class="section" style="display:none;">
                                  <div class="form-row rnd-6   saved"> <span class="clear-section"><i title="Supprimer" rel="txtTooltip" data-toggle="tooltip" data-placement="top" class="clear-section-row fa fa-times"></i></span> <i style="display:none;" data-direction="up" class="form-row-sorter fa fa-chevron-up" aria-hidden="true"></i> <i style="display:none;" data-direction="down" class="form-row-sorter fa fa-chevron-down" aria-hidden="true"></i>
                                      <input type="hidden" class="type-row" value="achievements" id="type-row-achievements" />
                                      <p class="row full-width">
                                          <label for="achievements" class="label">Description</label> <span class="full"><textarea id="achievements1" name="achievements" class="form-control" rows="10" cols="80"></textarea> </span> </p>
                                      <input type="hidden" class="form-row-sort-holder" name="form_row_sorter[]" value="" />
                                      <hr> </div> <span class="hr-overlay"></span> <span class="form-row-tips open-tips" data-tip="archivements"><i class="fa fa-info"></i> Conseils</span> </div>
                          </div>
                      </div>
                      <div class="tip-banner" id="archivements" style="display:none;">
                          <div class="center">
                              <h3>Conseils</h3>
                              <ul>
                                  <li> <i class="fa fa-check"></i> <span>Veuillez ne mentionner que les éléments pertinents ou qui ont une valeur ajoutée pour l'emploi auquel vous souhaitez postuler.</span> </li>
                              </ul> <i class="fa fa-times close-tips" data-tip="archivements"></i> </div>
                      </div>
                  </div>
                  <div class="otherwise origin ordering-field" data-type="otherwise" style="display:none;">
                      <div class="center">
                          <div class="form-col">
                              <h2 class=""><i class="icon-extra"></i> <input class="input-title extra_title" maxlength="32" type="text" name="extra_title[]" placeholder="Entrez un titre" value="" /> <span class="title-holder" data-default="Anders, namelijk...">Autre, à savoir…</span> <div class="title-overlay"></div> <div class="RightFloat Commands"> <i data-direction="up" class="sorter fa fa-chevron-up"></i> <i data-direction="down" class="sorter fa fa-chevron-down"></i> </div><span class="section-counter"></span> <hr> </h2>
                              <div class="section" style="display:none;">
                                  <div class="form-row rnd-6"> <span class="remove-row otherwise-row"> </span>
                                      <input type="hidden" class="type-row" value="extra-row" data-id="0" id="type-row-extra" />
                                      <p class="row full-width">
                                          <label for="otherwise" class="label">Description</label> <span class="full"> <textarea name="extra_description[]" class="form-control" id="extra_description_1" rows="10" cols="80"></textarea> </span> </p>
                                      <input type="hidden" class="form-row-sort-holder" name="form_row_sorter[]" value="" />
                                      <hr> </div> <span class="hr-overlay"></span> <span class="form-row-tips open-tips" data-tip="otherwise"><i class="fa fa-info"></i> Conseils</span> </div>
                          </div>
                      </div>
                      <div class="tip-banner" id="otherwise" style="display:none;">
                          <div class="center">
                              <h3>Conseils</h3>
                              <ul>
                                  <li> <i class="fa fa-check"></i> <span>Nommez le titre de cette section corrigée dans le champ de saisie. Ce titre figure sur le CV.</span> </li>
                                  <li> <i class="fa fa-check"></i> <span>Assurez-vous que la section personnalisée reste pertinente par rapport au contenu de votre CV.</span> </li>
                                  <li> <i class="fa fa-check"></i> <span>La longueur de la section ajustée doit rester en équilibre avec le reste du CV.</span> </li>
                              </ul> <i class="fa fa-times close-tips" data-tip="otherwise"></i> </div>
                      </div>
                  </div>
              </div>
              <div class="extra-form-fields">
                  <div class="center">
                      <div class="form-group"> <i class="icon-extra-section"></i>
                          <select class="extra_field custom-select mb-no" name="extra_field">
                              <option selected>+ Ajouter une partie</option>
                              <option value="references">Références</option>
                              <option value="goal">Objectif</option>
                              <option value="itskills">Informatique</option>
                              <option value="assets">Atouts</option>
                              <option value="characteristics">Qualités</option>
                              <option value="courses">Cours</option>
                              <option value="certificates">Certifications</option>
                              <option value="internships">Stages</option>
                              <option value="additional">Activités extra-professionnelles</option>
                              <option value="achievements">Divers</option>
                              <option value="otherwise">Autre, à savoir…</option>
                          </select> <i class="custom-select-arrow"></i> </div>
                  </div>
              </div>
              <div class="form-footer">
                  <div class="center">
                      <div class="extra-controllers" style="display:none;"> <span class="clone-form-section" data-section="extra_section" title=""><i class="fa fa-plus"></i> Voeg extra sectie toe</span> </div>
                      <input type="hidden" name="step" value="2">
                      <p style="text-align:center;">
                          <button type="submit" class="btn-blue-fill btn-next-step"><i class="fa fa-lock"></i>Étape suivante<i class="fa fa-chevron-right"></i></button>
                      </p>
                      <p style="text-align:center;padding-bottom:3px;">* En cliquant sur « étape suivante », vous acceptez nos <a href="/conditions-generales" title="conditions générales" target="_blank">conditions générales</a> et notre <a href="/politique-de-confidentialite" title="politique de confidentialité" target="_blank">politique de confidentialité</a>.</p>
                  </div>
              </div>
      </div>
      </form>
      <div class="center svg-inits"> <i class="icon-profile init">init</i> <i class="icon-certificates init">init</i> <i class="icon-references init">init</i> <i class="icon-characteristics init">init</i> <i class="icon-achievements init">init</i> <i class="icon-extra init">init</i> </div>
</div>
</main>
    <footer class="clearfix" role="contentinfo">
        <div class="stats">
            <div class="container">
            <div class="col-md-6">
                <img src="assets/images/b2.png" alt="" style="width: 400px;"/>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-6 col-sm-3 col-xs-6 text-left"> <span class="count">400</span> <span class="label text-left">Clients satisfaits</span>
                    </div>
                    <div class="col-md-6 col-sm-3 col-xs-6 text-left"> <span class="count">115</span> <span class="label text-left">CV traduits</span>
                    </div>
                    <div class="col-md-6 col-sm-3 col-xs-6 text-left"> <span class="count">32</span> <span class="label text-left">CV crées</span>
                    </div>
                    <div class="col-md-6 col-sm-3 col-xs-6 text-left"> <span class="count">1702</span> <span class="label text-left">Visiteurs/jr</span> 
                    </div>
                    <div class="col-md-6 col-sm-3 col-xs-6 text-left"> <span class="count">400</span> <span class="label text-left">ClientsModèles CV<br> disponible</span>
                    </div>
                    <div class="col-md-6 col-sm-3 col-xs-6 text-left"> <span class="count">115</span> <span class="label text-left">CV Nouveautés par mois</span>
                    </div>
                </div>
            </div>
            </div>
        </div>
        <div class="footer-nav">
            <div class="container">
            <div class="row">
                <div class="col-md-3 footer-logo">
                <a href="index.html" title="CV" class="site-logo steplink" data-next-step="1">
                    <img src="assets/images/logo.png" srcset="" alt="CV"> </a>
                </div>
                <div class="col-md-9">
                <div class="row">
                    
                </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 footer-bottom">
                <hr /> <span class="copyright">&copy; 2019 cvwizard.fr</span> <a class="trustpilot-rating float-right"
                    rel="nofollow" href="https://nl.trustpilot.com/review/cvwizard.fr" target="_blank">

                    4,4/5 (1702 reviews)
                </a>
                <script type="application/ld+json">
                    {
                        "@context": "http://schema.org/",
                        "@type": "Website",
                        "name": "cvwizard.fr",
                        "aggregateRating": {
                            "@type": "AggregateRating",
                            "ratingValue": "4,4",
                            "bestRating": 5,
                            "ratingCount": 1702
                        }
                    }
                </script>
                </div>
            </div>
            </div>
        </div>
    </footer>
   
    

 <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
 <script src="/assets/js/jquery.validate.min.js" type="text/javascript"></script>
 <script src="/assets/js/ckeditor/ckeditor.js" type="text/javascript"></script>
 <script src="/assets/js/jquery-ui.min.js" type="text/javascript"></script>
 <script src="/assets/js/bootstrap.min.js"></script>
 <script src="/assets/js/fastclick.js"></script>
 <script src="/assets/js/min/jquery.fancybox.pack.min.js" type="text/javascript"></script>
 <script src="/assets/js/jquery.infieldlabel.min.js" type="text/javascript"></script>
 <script src="/assets/js/jquery.sumoselect.min.js" type="text/javascript"></script>
 <script src="/assets/js/device.min.js" type="text/javascript"></script>
 <script src="/assets/js/min/main.min.js" type="text/javascript"></script>
 <script src="/assets/js/min/step_2.min.js" type="text/javascript"></script>
 <script src="/assets/js/validators/step2.validator.js" type="text/javascript"></script>
 <script src="/assets/js/min/mobile.min.js" type="text/javascript"></script>
    
</body>

</html>