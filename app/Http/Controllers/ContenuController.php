<?php

namespace App\Http\Controllers;

use App\Models\Work;
use App\Models\Skill;
use App\Models\Language;
use App\Models\Education;
use App\Models\Internship;

use App\Models\Certificate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContenuController extends Controller
{
    public function create(){
        return view('cvpro.contenu',['identification_id'=>1]);
    }

    public function store(Request $request){
        dd($request->all());
        $profil = $request->profile;
        $reference = $request->reference_text;
        $goal = $request->goal;

        $edu_name= $request->edu_name;
        $edu_institution = $request->edu_institution;
        $edu_city = $request->edu_city;
        $edu_month_start = $request->edu_month_start;
        $edu_year_start = $request->edu_year_start;
        $edu_month_end = $request->edu_month_end;
        $edu_year_end = $request->edu_year_end;
        $edu_description = $request->edu_description;

        for($count = 0;$count < count($edu_name);$count++){
            $data = [
                'edu_name' => $edu_name[$count],
                'edu_institution' => $edu_institution[$count],
                'edu_city'  => $edu_city[$count],
                'edu_month_start'  => $edu_month_start[$count],
                'edu_year_start'  => $edu_year_start[$count],
                'edu_month_end'  => $edu_month_end[$count],
                'edu_year_end'  => $edu_year_end[$count],
                'edu_description'  => $edu_description[$count],
                'identification_id' => 1
            ];
            
            Education::insert($data);
            dump($data);
        }

        $work_function = $request->work_function;
        $work_employer = $request->work_employer;
        $work_city = $request->work_city;
        $work_month_start = $request->work_month_start;
        $work_year_start = $request->work_year_start;
        $work_month_end = $request->work_month_end;
        $work_year_end = $request->work_year_end;
        $work_description = $request->work_description;


        for($count = 0;$count < count($work_function);$count++){
            $data = [
                "work_function" => $work_function[$count],
                "work_employer" => $work_employer[$count],
                "work_city" => $work_city[$count],
                "work_month_start" => $work_month_start[$count],
                "work_year_start" => $work_year_start[$count],
                "work_month_end" => $work_month_end[$count],
                "work_year_end" => $work_year_end[$count],
                "work_description" => $work_description[$count],
                'identification_id' => 1
            ];
            Work::insert($data);
            dump($data);
        }
       
        $skill_skill = $request->skill_skill;
        $skill_level = $request->skill_level;

        for($count = 0;$count < count($skill_skill);$count++){
            $data = [
                "skill_skill" => $skill_skill[$count],
                "skill_level" => $skill_level[$count],
                'identification_id' => 1
            ];
            Skill::insert($data);
            dump($data);
        }

        $language_language = $request->language_language;
        $language_level = $request->language_level;

        for($count = 0;$count < count($language_language);$count++){
            $data = [
                "language_language" => $language_language[$count],
                "language_level" => $language_level[$count],
                'identification_id' => 1
            ];
            Language::insert($data);
            dump($data);
        }

        $hobby_hobby = $request->hobby_hobby;

        for($count = 0;$count < count($hobby_hobby);$count++){
            $data = [
                "hobby_hobby" => $hobby_hobby[$count]
            ];

            dump($data);
        }

        $certificates_name = $request->certificates_name;
        $certificates_year = $request->certificates_year;
        $certificates_finished = $request->certificates_finished;
        $certificates_description = $request->certificates_description;

        for($count = 0;$count < count($certificates_name);$count++){
            $data = [
                "certificates_name" => $certificates_name[$count],
                "certificates_year" => $certificates_year[$count],
                "certificates_finished" => $certificates_finished[$count],
                "certificates_description" => $certificates_description[$count],
                'identification_id' => 1
            ];
            Certificate::insert($data);
            dump($data);
        }

        $course_name = $request->course_name;
        $course_year_end = $request->course_year_end;
        $course_finished = $request->course_finished;
        $course_description = $request->course_description;

        for($count = 0;$count < count($course_name);$count++){
            $data = [
                "course_name" => $course_name[$count],
                "course_year_end" => $course_year_end[$count],
                "course_finished" => $course_finished[$count],
                "course_description" => $course_description[$count],
                'identification_id' => 1
            ];
            Course::insert($data);
            dump($data);
        }


        $internship_function = $request->internship_function;
        $internship_employer = $request->internship_employer;
        $internship_city = $request->internship_city;
        $internship_month_start = $request->internship_month_start;
        $internship_year_start = $request->internship_year_start;
        $internship_month_end = $request->internship_month_end;
        $internship_year_end = $request->internship_year_end;
        $internship_description = $request->internship_month_end;

        for($count = 0;$count < count($internship_function);$count++){
            $data = [
                "internship_function" => $internship_function[$count],
                "internship_employer" => $internship_employer[$count],
                "internship_city" => $internship_city[$count],
                "internship_month_start" => $internship_month_start[$count],
                "internship_year_start" => $internship_year_start[$count],
                "internship_month_end" => $internship_month_end[$count],
                "internship_year_end" => $internship_year_end[$count],
                "internship_description" => $internship_description[$count],
                'identification_id' => 1
            ];
            Internship::insert($data);
            dd($data);
        }
    }
}
