<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Identification;

class IdentificationController extends Controller
{
    public function create(){
        return view('cvpro.identification');
    }

    public function store(Request $request){
        $data = $request->all();
        dump($data);
        $identification = new Identification();

        $identification->firstname = $request->input('firstname');
        $identification->lastname = $request->input('lastname');
        $identification->telephone = $request->input('phonenumber');
        $identification->email = $request->input('email');
        $identification->birthday = $request->input('dob_day');
        $identification->birthday_month = $request->input('dob_month');
        $identification->birthday_year = $request->input('dob_year');
        $identification->situation_matrimonial = $request->input('maritalstatus_text');
        $identification->nationalite = $request->input('nationality');
        $identification->lieu_de_naissance = $request->input('dob_city');
        $identification->linkedin = $request->input('linkedin');
        $identification->url = $request->input('url');
        $identification->permis_de_conduire = $request->input('drivinglicense_text');
        $identification->ville = $request->input('city');
        $identification->profil = $request->input('phonenumber');
        $identification->addresse = $request->input('street');
        $identification->code_postal = $request->input('zipcode');

        if($identification->save()){
            return \Redirect::route('contenu',['identification_id'=>$identification->id]);
            dump($identification->id);
            dd($identification); 
        }
        dd($identification);

    }


    public function edit(Identification $identification) {

    }

    public function update(Request $request,Identification $identification){

    }
}
