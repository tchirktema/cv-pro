<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    protected $fillable = [
        "identification_id",
        "language_language" ,
        "language_level" ,
    ];
}
