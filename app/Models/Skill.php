<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Skill extends Model
{
    protected $fillable = [
        "skill_skill" ,
        "skill_level" ,
        "identification_id"
    ];
}
