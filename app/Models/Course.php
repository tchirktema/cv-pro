<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = [
        "course_name" ,
        "course_year_end" ,
        "course_finished" ,
        "course_description" ,
        "identification_id"
    ];
}
