<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Work extends Model
{
    protected $fillable = [
        "work_function",
        "work_employer",
        "work_city",
        "work_month_start",
        "work_year_start",
        "work_month_end",
        "work_year_end",
        "work_description",
        "identification_id"
    ];
}
