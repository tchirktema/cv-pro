<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Identification extends Model
{
    protected $fillable = ['firstname','lastname','telephone','email','birthday',
        'birthday_month','birthday_year','situation_matrimonial','nationalite',
        'lieu_de_naissance','linkedin','url','permis_de_conduire','ville'
        ,'profil','addresse','code_postal'];
}
