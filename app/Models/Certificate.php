<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Certificate extends Model
{
    protected $fillable = [
        "certificates_name" ,
        "certificates_year" ,
        "certificates_finished" ,
        "certificates_description" ,
        "identification_id"
    ];
}
