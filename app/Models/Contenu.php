<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contenu extends Model
{
    protected $fillable = [
        'identification_id',
        'profil',
        'goal',
        'references',
        
    ];
}
