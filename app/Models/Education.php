<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Education extends Model
{
    protected $fillable = [
        'edu_name' ,
        'edu_institution',
        'edu_city',
        'edu_month_start',
        'edu_year_start',
        'edu_month_end',
        'edu_year_end',
        'edu_description',
        'identification_id'
    ];
}
