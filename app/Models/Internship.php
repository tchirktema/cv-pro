<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Internship extends Model
{
    protected $fillable = [
        "internship_function" ,
        "internship_employer" ,
        "internship_city" ,
        "internship_month_start" ,
        "internship_year_start" ,
        "internship_month_end" ,
        "internship_year_end" ,
        "internship_description" ,
        "identification_id"
        
    ];
}
