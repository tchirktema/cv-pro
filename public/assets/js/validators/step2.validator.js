/**
 *
 * Validator class step2
 */
(function ($) {
    $.Validator = function (element) {
        this.element = (element instanceof $) ? element : $(element);
        this.enableDebugging = false;
    };

    $.Validator.prototype = {
        InitEvents: function () {
            var functions = this;

            // Validate on input change
            var timer_dw8128h12 = null;
            $('form#step_2 input.raw').keyup(function (e) {
                var element = $(this);
                clearTimeout(timer_dw8128h12);
                timer_dw8128h12 = setTimeout(function(){
                    var subForm = $(element).parents('.form-row.active');
                    functions.removeUntouchedClass(subForm, 'remove untouched from input key up');

                    if (!$(element).hasClass('validate-no')) {
                        functions.preValidatSubForm(subForm);
                        functions.validateSubForm(subForm, 'input', 'validate on input key up');
                        functions.sectionHeaderError(subForm.parents('.form-col'));
                    }
                    ///functions.countValidSubForms(subForm.parents('.section'), 'count validate sub forms from key up');
                }, 150);
            });

            // Validate on tab
            $('form#step_2 input.raw').keydown(function (e) {
                var keyCode = e.keyCode || e.which;
                var element = $(this);
                if (keyCode == 9) {
                    var subForm = $(this).parents('.form-row.active');
                    functions.removeUntouchedClass(subForm, 'remove untouched from input click');
                    if (!$(element).hasClass('validate-no')) {
                        functions.validateSubForm(subForm, 'input', 'validate on input click');
                        functions.sectionHeaderError(subForm.parents('.form-col'));
                    }
                }
            });

            $('form#step_2 .input-title').click(function(){
                $(document).find('.extra-row-send').removeClass('extra-row-send');
            });

            // Validate on input change
            $('form#step_2 input.raw').click(function (e) {
                var subForm = $(this).parents('.form-row.active');

                functions.removeUntouchedClass(subForm, 'remove untouched from input click');
                functions.validateSubForm(subForm, 'input', 'validate on input click');
                functions.sectionHeaderError(subForm.parents('.form-col'));
            });

            // Validate on select change
            var timer_dw8128h12 = null;
            $('form#step_2 select.raw').change(function (e) {
                var element = $(this);
                clearTimeout(timer_dw8128h12);
                timer_dw8128h12 = setTimeout(function(){
                    var subForm = $(element).parents('.form-row.active');
                    functions.removeUntouchedClass(subForm, 'remove untouched from input key up');
                    functions.validateSubForm(subForm, 'select', 'validate on input key up');
                    functions.sectionHeaderError(subForm.parents('.form-col'));
                }, 50);
            });

            // Ckeditor changes
            var timer_dw8128h28 = null;
            CKEDITOR.on('instanceReady', function(evt) {
                evt.editor.on('change', function () {
                    clearTimeout(timer_dw8128h28);
                    var editorId = $(this).attr('name');
                    var subForm  = $('textarea#' + editorId).parents('.form-row.active');
                    var parent   = $('textarea#' + editorId).parents('.section');
                    var textareaOnlyFormRows = ['achievements', 'profile', 'otherwise'];
                    timer_dw8128h28 = setTimeout(function() {
                        $(document).removeClass('extra-row-send');
                        functions.removeUntouchedClass(subForm, 'remove untouched from ckeditor');
                        functions.validateSubForm(subForm, 'ckeditor', 'validate on ckeditor');
                        functions.sectionHeaderError(subForm.parents('.form-col'));
                        CKEDITOR.instances[editorId].updateElement();
                        $(parent).find('.saved').removeClass('saved');
                        $(textareaOnlyFormRows).each(function(index, value){
                            if ($('textarea#' + editorId).parents('.ordering-field').hasClass(value)) {
                                if (CKEDITOR.instances[editorId].getData().length > 0) {
                                    $('textarea#' + editorId).addClass('has-data');
                                    if (value == 'otherwise') {
                                        if ($(parent).prev().find('.input-title').val()) {
                                            $(parent).prev().find('.section-counter').text('(1)');
                                            $(parent).find('.form-row').addClass('row-valid');
                                        } else {
                                            $(parent).prev().find('.section-counter').text('');
                                            $(parent).find('.form-row').removeClass('row-valid');
                                        }
                                        $(document).find('.extra-row-send').removeClass('extra-row-send');
                                        //$(document).find('.saved').removeClass('saved');
                                    } else {
                                        $(parent).prev().find('.section-counter').text('(1)');
                                        $(parent).find('.form-row').addClass('row-valid');
                                    }
                                } else {
                                    $(parent).prev().find('.section-counter').text('');
                                    $(parent).find('.form-row').removeClass('row-valid');
                                    $('textarea#' + editorId).removeClass('has-data');
                                }
                            }
                        });
                    }, 500);
                });
            });

            // Validate on form removal
            $('form#step_2 .remove-row').click(function (e){

                $(document).find('.otherwise .saved').removeClass('saved');
                $(document).find('.otherwise.extra-row-send').removeClass('extra-row-send');

                $('form#step_2 .form-row').each(function(){
                    var subForm = $(this);

                    // functions.validateSubForm(subForm, 'validate on select change');
                    functions.sectionHeaderError(subForm.parents('.form-col'));
                    functions.countValidSubForms(subForm.parents('.section'), 'count validate sub forms on select form');
                });
            });

            // Validate on form reset
            $('form#step_2 .clear-section').click(function (e){
                $(this).parent().hide();
                $('form#step_2 .form-row').each(function(){
                    var subForm = $(this);
                    // functions.validateSubForm(subForm, 'validate on select change');
                    functions.sectionHeaderError(subForm.parents('.form-col'));
                    functions.countValidSubForms(subForm.parents('.section'), 'count validate sub forms on select form');
                });
            });

            // Validate on form row click
            $('form#step_2 .form-row').click(function (e){

                if ($(e.target).is('input') || $(e.target).is('select') || $(e.target).is('option')) {
                    return true;
                } else {
                    $(this).parents('.section').find('.form-row').each(function(){
                        var subForm = $(this);
                        if (functions.subFormContainsData(subForm) && !$(subForm).hasClass('active')) {
                            subForm.removeClass('untouched hold-validation');
                        }
                        functions.validateSubForm(subForm, 'form-row', 'validate on switching to other form row');
                        functions.sectionHeaderError(subForm.parents('.form-col'));
                        functions.countValidSubForms(subForm.parents('.form-col'));
                    });
                }
            });

            // Validate on clone form row
            $('form#step_2 .clone-form-row').click(function (e){
                $(this).parents('.section').find('.form-row').each(function () {
                    var subForm = $(this);
                    if (functions.subFormContainsData(subForm)) {
                        subForm.removeClass('untouched hold-validation');
                    }
                    functions.validateSubForm(subForm, 'clone-form-row', 'validate on switching to other form row');
                    functions.sectionHeaderError(subForm.parents('.form-col'));
                    functions.countValidSubForms(subForm.parents('.form-col'), 'Count the valid rows now!');
                });
            });

            // Validate on section click
            $('form#step_2 .ordering-field:not(.otherwise) .form-col h2').click(function (e) {

                $(this).parents('form#step_2').find('.form-row').each(function(){
                    var subForm = $(this);
                    if (functions.subFormContainsData(subForm) && !$(subForm).hasClass('untouched')) {
                        subForm.removeClass('untouched hold-validation');
                    }
                    functions.validateSubForm(subForm, 'heading', 'validate on switching to other form row');
                    functions.sectionHeaderError(subForm.parents('.form-col'));
                    functions.countValidSubForms(subForm.parents('.form-col'));
                });
            });

            // Validate on form submit
            $('form#step_2 button, a#step_3_link, a#step_1_link').click(function (e) {
                e.preventDefault();
                $('form#step_2').find('.form-row').each(function(){
                    var subForm = $(this);
                    if (functions.subFormContainsData(subForm)) {
                        subForm.removeClass('untouched hold-validation');
                    }
                    subForm.find('.saved').removeClass('saved');
                    subForm.find('.extra-row-send').removeClass('extra-row-send');
                    functions.validateSubForm(subForm, 'submit', 'validate on switching to other form row');
                    functions.sectionHeaderError(subForm.parents('.form-col'));
                    functions.countValidSubForms(subForm.parents('.form-col'));
                });
            });
        },
        /**
         *
         * Pre validation when user fills required input directly
         * @param element
         */
        preValidatSubForm: function (element, debugMsg) {

            // Set variables
            var formInputElements               = $(element).find('input.raw');
            var formSelectElements              = $(element).find('select.raw');
            var formTextareaElements            = $(element).find('textarea.ckeditor');
            var formRequiredElements            = $(element).find('.raw:not(.validate-no)').length;
            var countRequiredTotalFormElements  = 0;
            var countTotalFormElementsWithValue = 0;

            // Loop through form input elements
            $(formInputElements).each(function() {
                if ($(this).val()) {
                    countRequiredTotalFormElements++;
                    countTotalFormElementsWithValue++;
                }
            });

            // Loop through form select elements
            $(formSelectElements).each(function() {
                if (this.selectedIndex) {
                    countRequiredTotalFormElements++;
                    countTotalFormElementsWithValue++;
                }
            });

            // Has textarea and value?
            $(formTextareaElements).each(function() {
                if (CKEDITOR.instances[$(this).attr('id')].getData() !== '') {
                    countRequiredTotalFormElements++;
                    countTotalFormElementsWithValue++;
                }
            });

            // Filled amount of required form elements
            if (formRequiredElements == countRequiredTotalFormElements) {
                $(element).addClass('row-valid');
                $(element).removeClass('untouched hold-validation saved');
            }
        },
        /**
         *
         * Validate current form
         * @param element
         * @param clickedElement
         */
        validateSubForm: function (element, clickedElement, debugMsg) {

            // Hold validation
            if ($(element).hasClass('hold-validation')) {
                return false;
            }

            // Set variables
            var formInputElements               = $(element).find('input.raw');
            var formSelectElements              = $(element).find('select.raw');
            var formTextareaElements            = $(element).find('textarea.ckeditor');
            var formRequiredElements            = $(element).find('.raw:not(.validate-no)').length;
            var countTotalFormElements          = formInputElements.length + formSelectElements.length + formTextareaElements.length;
            var countRequiredTotalFormElements  = 0;
            var countTotalFormElementsWithValue = 0;

            // Remove saved class
            if (clickedElement !== 'heading' && clickedElement !== 'submit' && clickedElement !== 'ckeditor') {
                ///$(element).removeClass('saved');
            }

            // Loop through form input elements
            $(formInputElements).each(function() {
                if (!$(this).val()) {
                    if (!$(this).hasClass('validate-no')) {
                        $(this).addClass('error');
                    }
                } else {
                    if (!$(this).hasClass('validate-no')) {
                        countRequiredTotalFormElements++;
                    }
                    countTotalFormElementsWithValue++;
                    $(this).removeClass('error');
                }
            });

            // Loop through form select elements
            $(formSelectElements).each(function() {
                if (!this.selectedIndex) {
                    if (!$(this).hasClass('validate-no')) {
                        $(this).addClass('error');
                    }
                } else {
                    if (!$(this).hasClass('validate-no')) {
                        countRequiredTotalFormElements++;
                    }
                    countTotalFormElementsWithValue++;
                    $(this).removeClass('error');
                }
            });

            // Has textarea and value?
            $(formTextareaElements).each(function() {
                if (CKEDITOR.instances[$(this).attr('id')].getData() !== '') {
                    countTotalFormElementsWithValue++;
                }
            });

            // Filled amount of required form elements
            if (formRequiredElements !== countRequiredTotalFormElements) {
                if (this.enableDebugging) {
                    console.log('filled required form elements => FAILED!');
                }
                $(element).removeClass('row-valid');
            } else {
                if (this.enableDebugging) {
                    console.log('filled required form elements => OK!');
                }
                $(element).addClass('row-valid');
            }

            // All form elements emptied, reset form
            if (!countTotalFormElementsWithValue) {
                $('.raw', element).removeClass('error');
                $(element).addClass('untouched');
                $(element).removeClass('contains-data');
            } else {
                $(element).addClass('contains-data');
            }

            if (this.enableDebugging) {
                console.log('total form elements => ', countTotalFormElements);
                console.log('total form elements filled => ', countTotalFormElementsWithValue);
                console.log('Required form elements => ', formRequiredElements);
                console.log('Required form elements filled in => ', countRequiredTotalFormElements);
                console.log('------------------[ ./validate outputs ]------------------');
            }
        },
        /**
         *
         * Section heading error
         */
        sectionHeaderError: function(element, debugMsg) {
            if ($('.raw.error', element).length > 0) {
                $(element).find('h2').addClass('error');
            } else {
                $(element).find('h2').removeClass('error');
            }
        },
        /**
         *
         * Validate terms
         */
        validateTerms: function(debugMsg) {
            return true;
        },
        /**
         *
         * Count valid sub forms
         */
        countValidSubForms: function (element, debugMsg) {
            var validRows = $('.row-valid:not(.untouched)', element).length;
            if (validRows > 0) {
                $(element).parent().parent().find('.section-counter').text('(' + validRows + ')');
            } else {
                if ($(element).find('input#reference_on_request').is(':checked')) {
                    $(element).parent().parent().find('.section-counter').text('(1)');
                } else {
                    $(element).parent().parent().find('.section-counter').text('');
                }
            }
            if (this.enableDebugging) {
                console.log(debugMsg);
            }
        },
        /**
         *
         * Remove untouched form class
         */
        removeUntouchedClass: function (element, debugMsg) {
            $(element).removeClass('untouched');
            if (this.enableDebugging) {
                console.log(debugMsg);
            }
        },
        /**
         *
         * Subform contains data
         */
        subFormContainsData: function (element, debugMsg) {

            // Count inputs with data
            var formInputWithValue = $(element).find('input.raw').filter(function () {
                return !!this.value;
            }).length;

            // Count selects with data
            var formSelectWithValue = $(element).find('select.raw').filter(function () {
                if (this.selectedIndex) {
                    return !!this.value;
                }
            }).length;

            // Has textarea and value?
            var formTextareaElements  = $(element).find('textarea.ckeditor');
            var formTextareaWithValue = false;
            $(formTextareaElements).each(function() {
                if (CKEDITOR.instances[$(this).attr('id')].getData() !== '') {
                    formTextareaWithValue = true;
                }
            });

            if (this.enableDebugging) {
                console.log(debugMsg);
            }

            return (formInputWithValue || formSelectWithValue || formTextareaWithValue) ? true : false;
        },
        /**
         *
         * Check if form contains errors
         */
        formIsValid : function (debugMsg) {
            return ($('.error').length) ? false : true;
        }
    };

}(jQuery));

// Init validator
var validator = new $.Validator();
validator.InitEvents();