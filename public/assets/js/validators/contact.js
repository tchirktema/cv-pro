$(document).ready(function(){

    // Validate contact form
    $('input').on('change keyup', function (e) {
    	if ($(this).hasClass('error')) {
            formValidation(e);
		}
    });

    $('select').on('change', function (e) {
        if ($(this).hasClass('error')) {
            formValidation(e);
        }
    });

    $('textarea').on('keyup', function (e) {
        if ($(this).hasClass('error')) {
            formValidation(e);
        }
    });

    $('input, select, textarea').on('focus', function (e) {
        if ($(this).hasClass('error')) {
            $('.error-container').not($(this).prev()).hide();
            $(this).prev().show();
        }
    });

    $('form.form-stnd button').click(function (e) {
    	if (!formValidation(e)) {
            e.preventDefault();
		}
    });

});

function formValidation(e) {
    var isValid = true;

    if (!$('form.form-stnd').hasClass('validated')) {
        $('form.form-stnd').addClass('validated');
    }

    $('input.required, select.required, textarea.required').each(function(){

        var emailRegex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;

        // Validate empty
        if (!$(this).val() && $(this).attr('id') !== 'email') {

            isValid = false;
            $(this).addClass('error');

        } else if ($(this).attr('id') == 'email' && !emailRegex.test($('input#email').val())) {

            isValid = false;
            $(this).addClass('error');

        } else {
            $(this).prev().hide();
            $(this).removeClass('error');
        }

    });
    return isValid;
}

