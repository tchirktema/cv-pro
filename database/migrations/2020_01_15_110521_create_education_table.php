<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEducationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('education', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('edu_name');
            $table->string('edu_institution');
            $table->string('edu_city');
            $table->string('edu_month_start');
            $table->string('edu_year_start');
            $table->string('edu_month_end');
            $table->string('edu_year_end');
            $table->string('edu_description');
            $table->integer('identification_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('education');
    }
}
