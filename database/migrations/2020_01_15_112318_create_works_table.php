<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('works', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('work_function');
            $table->string('work_employer');
            $table->string('work_city');
            $table->string('work_month_start');
            $table->string('work_year_start');
            $table->string('work_month_end');
            $table->string('work_year_end');
            $table->text('work_description')->nullable();
            $table->integer('identification_id')->unsigned();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('works');
    }
}
