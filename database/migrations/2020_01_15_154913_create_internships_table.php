<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInternshipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('internships', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('identification_id')->unsigned();
            $table->string('internship_function');
            $table->string('internship_employer');
            $table->string('internship_city');
            $table->string('internship_month_start');
            $table->string('internship_year_start');
            $table->string('internship_month_end');
            $table->string('internship_year_end');
            $table->string('internship_description');
            $table->string('course_description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('internships');
    }
}
