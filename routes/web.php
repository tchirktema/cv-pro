<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('cvpro.index');
});


Route::get('/creer-un-cv','IdentificationController@create')->name('cvpro.identification');
Route::post('/creer-un-cv','IdentificationController@store')->name('identification.store');


Route::get('/contenu','ContenuController@create')->name('cvpro.contenu');
Route::post('/contenu','ContenuController@store')->name('contenu.store');